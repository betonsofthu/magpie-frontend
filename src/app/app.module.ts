import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';


import {AppRoutingModule} from './app.routing';
import {ComponentsModule} from './components/components.module';

import {AppComponent} from './app.component';
import {
    AgmCoreModule
} from '@agm/core';
import {UserLayoutComponent} from './layouts/user-layout/user-layout.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
} from '@angular/material';
import {ProgressBarModule} from './main/progress-bar/progress-bar.module';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientJsonpModule, HttpClientModule} from '@angular/common/http';
import {TokenInterceptor} from './main/service/rest/interceptor/token-interceptor';
import {LoggingInterceptor} from './main/service/rest/interceptor/logging-interceptor';
import {ProgressBarHandlerInterceptor} from './main/service/rest/interceptor/progress-bar-handler-interceptor.service';
import {CookieService} from 'ngx-cookie-service';
import {AuthenticationModule} from './main/authentication/authentication.module';
import {NotificationModule} from './main/notification/notification.module';
import {UsersModule} from './main/users/users.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {environment} from '../environments/environment';
import { SocialLoginModule, AuthServiceConfig } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider } from "angularx-social-login";
 

export function getAuthServiceConfigs() {
    let config = new AuthServiceConfig(
        [
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider("Your-Facebook-app-id")
          },
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider("610635952942-07iem67cou7a352aflr1lv5nocj2nrqv.apps.googleusercontent.com")
          },
        ]
    );
    return config;
  }


@NgModule({
    imports: [
        FlexLayoutModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        HttpClientJsonpModule,
        ComponentsModule,
        RouterModule,
        AppRoutingModule,
        AgmCoreModule.forRoot({
            apiKey: 'YOUR_GOOGLE_MAPS_API_KEY'
        }),
        ProgressBarModule,
        AuthenticationModule,
        NotificationModule,
        UsersModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        SocialLoginModule
    ],
    declarations: [
        AppComponent,
        UserLayoutComponent
    ],
    providers: [
        {provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: LoggingInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: ProgressBarHandlerInterceptor, multi: true},
        CookieService,
        {
            provide: AuthServiceConfig,
            useFactory: getAuthServiceConfigs
          }
    ],
    bootstrap: [AppComponent]
})



export class AppModule {
}

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, environment.restURL + '/api/', '/langs/getLines');
}
