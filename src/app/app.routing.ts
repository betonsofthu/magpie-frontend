import {NgModule} from '@angular/core';
import {CommonModule,} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {Routes, RouterModule} from '@angular/router';

import {AuthGuard} from './main/guards/auth.guard';
import {LoginComponent} from './main/authentication/login/login.component';
import {UserLayoutComponent} from './layouts/user-layout/user-layout.component';
import {RegisterComponent} from './main/authentication/register/register.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
    },
    {
        path: '',
        component: UserLayoutComponent,
        children: [
            {
                path: '',
                loadChildren: './layouts/user-layout/user-layout.module#UserLayoutModule',
            }
        ],
        canActivate: [AuthGuard]
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'register',
        component: RegisterComponent
    }
];

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule.forRoot(routes)
    ],
    exports: [],
})
export class AppRoutingModule {
}
