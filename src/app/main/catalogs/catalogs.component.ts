import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CatalogService } from '../service/catalog/catalog.service';

@Component({
  selector: 'app-catalogs',
  templateUrl: './catalogs.component.html',
  styleUrls: ['./catalogs.component.scss']
})
export class CatalogsComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'menu'];
  catalogs = [];


  constructor(private router: Router,
    private catalogService: CatalogService) { }

  ngOnInit() {
    this.init();
  }

  init(){
    this.catalogService.getAllCatalogs().subscribe((catalogs) => {
      this.catalogs = catalogs;
    });
  }

  addEditCatalog(catalogId?: number) {
    if (catalogId){
      this.router.navigate(['catalogs/add-edit'], { queryParams: { catalogId: catalogId} });
    }else{
      this.router.navigate(['catalogs/add-edit']);
    }
  }

  deleteCatalog(catalogId: number) {
     this.catalogService.removeCatalog(catalogId).subscribe(() => {
        this.init();
     });
  }

  clickCatalog(catalog) {
    this.addEditCatalog(catalog.id);
  }

}
