import { Component, OnInit, ViewChild } from '@angular/core';
import { CatalogService, Catalog } from 'app/main/service/catalog/catalog.service';
import { isNullOrUndefined } from 'util';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator, MatTableDataSource, PageEvent } from '@angular/material';
import { NotificationService } from 'app/main/notification/notification.service';



@Component({
  selector: 'app-add-edit-catalog',
  templateUrl: './add-edit-catalog.component.html',
  styleUrls: ['./add-edit-catalog.component.scss']
})
export class AddEditCatalogComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;

  title: string;

  catalogs = [];

  sysColumns: any[] = [];
  fileCols: any[] = [];

  catalog: Catalog;
  file: File;

  displayedColumns: string[] = ['id', 'title', 'condition', 'quantity', 'discount', 'price', 'price_estimated', 'menu'];
  dataSource = new MatTableDataSource<any>([]);
  products: any[] = [];
  total = 0;
  pageIndex = 0;
  pageSize = 10;

  indexExpanded: number = 1;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private catalogService: CatalogService,
    private notifyService: NotificationService) { }

  ngOnInit() {
    this.catalog = <Catalog>{ id: null, title: "" };
    this.dataSource.paginator = this.paginator;
    this.route.queryParams.subscribe(params => {
      var that = this;
      if (params.catalogId && params.catalogId > -1){
        this.catalogService.getCatalog(params.catalogId).subscribe((catalog) => {
          that.catalog = catalog;
          this.getProducts(this.pageSize);
        });
      }
    });
  }

  getProducts(limit, page = 1) {
    this.catalogService.getProducts(this.catalog.id, limit, page).subscribe((products) => {
      this.products = products;
      this.paginator.length = products.total;
      this.paginator.pageIndex = products.current_page - 1;
      this.dataSource = new MatTableDataSource<any>(products.data);
    });
  }

  paginatorChanged(event: PageEvent) {
    this.getProducts(event.pageSize, event.pageIndex + 1);
  }

  saveCatalog() {
    if (isNullOrUndefined(this.catalog.id)){
      this.catalogService.saveCatalog(this.catalog.title).subscribe((catalog) => {
        this.catalog = catalog;
        this.notifyService.success("Save catalog was successful!");
      });
    }else{
      this.catalogService.updateCatalog(this.catalog.id, this.catalog.title, this.catalog.active ? 1: 0).subscribe((catalog) => {
        this.catalog = catalog;
        this.notifyService.success("Update catalog was successful!");
      });
    }
   
  }

  uploadFile() {
    if (!isNullOrUndefined(this.catalog) && !isNullOrUndefined(this.file)) {
      this.catalogService.uploadFile(this.catalog.id, this.file).subscribe((mappingData) => {
        this.sysColumns = mappingData.products_fields.map((prodField) => {
          return { value: null, col: prodField };
        });
        this.fileCols = mappingData.file_fields;
      });
    }
  }

  saveMapping() {

    const mappedFields = {};
    this.sysColumns.forEach((sCol) => {
      if (sCol.value != null) {
        mappedFields[sCol.col.name] = sCol.value;
      }
    });

    this.catalogService.saveMapping(this.catalog.id, mappedFields).subscribe((products) => {
      this.setStep(1);
      this.getProducts(this.pageSize);
    });
  }

  setStep(step) {
    this.indexExpanded = step;
  }

  onFileSelected(event) {
    this.file = (<HTMLInputElement>event.target).files[0];
  }

  clickProduct(product) {
    console.log(product);
    this.router.navigate(['/crud-prod'], { queryParams: { catalogId: this.catalog.id, productId: product.id } });
  }

  removeProduct(productId) {
    this.catalogService.removeProduct(productId).subscribe(() =>{
      this.getProducts(this.pageSize);
    });
  }
}
