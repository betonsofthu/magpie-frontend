import { Component, OnInit, ViewChild } from '@angular/core';
import { CatalogService, ProductImage } from 'app/main/service/catalog/catalog.service';
import { ActivatedRoute, Router } from '@angular/router';
import { isNullOrUndefined } from 'util';
import { NotificationService } from 'app/main/notification/notification.service';
import { MatHorizontalStepper } from '@angular/material';
import { ProductImageViewerComponent } from 'app/main/shared/product-image-viewer/product-image-viewer.component';
import { SharedService } from 'app/main/service/shared.service';
import { CompaniesService } from 'app/main/service/companies/companies.service';
import { Location } from '@angular/common';



@Component({
  selector: 'app-crud-product',
  templateUrl: './crud-product.component.html',
  styleUrls: ['./crud-product.component.scss']
})
export class CrudProductComponent implements OnInit {

  @ViewChild('stepper') stepper: MatHorizontalStepper;
  @ViewChild('viewer') viewer: ProductImageViewerComponent;

  isLinear = true;
  catalogId: number;
  productId: number;

  imgsFormId: number;
  formId: number;

  productFields: any[] = [];
  productImages: ProductImage[] = [];

  constructor(private catalogService: CatalogService,
    private route: ActivatedRoute,
    private router: Router,
    private notifyService: NotificationService,
    private location: Location) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.catalogId = params.catalogId;
      this.productId = params.productId;
      var that = this;
      this.catalogService.getProduct(this.productId).subscribe((productFields) => {
        this.formId = productFields.formId;
        this.productFields = productFields.fields;
      });

      this.catalogService.getProductImages(this.productId).subscribe((imageFields) => {
        this.imgsFormId = imageFields.formId;
        this.productImages = imageFields.fields;
      });

    });
  }

  imgChangeListener(prodImage: ProductImage, $event: Event) {
    const reader = new FileReader();
    const file = (<HTMLInputElement>event.target).files[0];
    reader.onloadend = function (e) {
      const src = reader.result;
      prodImage.image = src;
    };
    if (file && file.type.match('image.*')) {
      reader.readAsDataURL(file);
      prodImage.path = file.name;
      prodImage.file = file;
    }
  }

  saveProduct(): void {
    const fields = { form: this.formId };
    this.productFields.forEach(field => {
      fields[field.name] = field.value;
    });
    this.catalogService.saveProduct(this.catalogId, fields, this.productId).subscribe((prodId) => {
      this.productId = prodId;
      this.notifyService.success('Saving product was successful!');
      this.stepper.selected.completed = true;
      this.stepper.selected.editable = true;
      this.stepper.next();
    });

  }

  saveProductImages(): void {
    const formData = new FormData();
    formData.append("form", this.imgsFormId.toString());
    let isAddedImg = false;
    this.productImages.forEach(field => {
      if (!isNullOrUndefined(field.file)) {
        formData.append(field.name, field.file, field.file.name);
      } else {
        if (!field.path){
          formData.append(field.name, "");
        }
      }
    });
    this.catalogService.saveProduct(this.catalogId, formData, this.productId).subscribe(() => {
      this.notifyService.success('Saving product images was successful!');
      this.router.navigate(['catalogs/add-edit'], { queryParams: { catalogId: this.catalogId} });
    });
  }

  prodImgViewer(prodImg: ProductImage) {
    if (isNullOrUndefined(prodImg.image) || !prodImg.image) {
      return;
    }
    this.viewer.open(0, [prodImg.image]);
  }

  previewAll(): void {
    const imgs = [];
    this.productImages.forEach((prodImage) => {
      if (!isNullOrUndefined(prodImage.image) && prodImage.image) {
        imgs.push(prodImage.image);
      }
    });
    if (imgs.length > 0) {
      this.viewer.open(0, imgs);
    }

  }

  emptyImage(prodImg: ProductImage): void {
    prodImg.file = null;
    prodImg.image = null;
    prodImg.path = null;
  }

  saveImages(): void {

  }
}
