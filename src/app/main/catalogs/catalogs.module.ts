import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogsComponent } from './catalogs.component';
import { MatFormFieldModule, MatInputModule, MatCardModule, MatIconModule, MatButtonModule, MatTableModule, MatMenuModule, MatSelectModule, MatExpansionModule, MatStepperModule, MatPaginatorModule, MatCheckboxModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { AddEditCatalogComponent } from './add-edit-catalog/add-edit-catalog.component';
import { AppRoutingModule } from 'app/app.routing';
import { RouterModule } from '@angular/router';
import { CrudProductComponent } from './add-edit-catalog/crud-product.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    TranslateModule,
    MatMenuModule,
    MatSelectModule,
    MatExpansionModule,
    RouterModule,
    MatStepperModule,
    MatPaginatorModule,
    MatCheckboxModule,
    SharedModule
  ],
  declarations: [
    CatalogsComponent,
    AddEditCatalogComponent,
    CrudProductComponent
  ],
  exports: [
    CatalogsComponent,
    AddEditCatalogComponent,
    CrudProductComponent
  ]
})
export class CatalogsModule { }

