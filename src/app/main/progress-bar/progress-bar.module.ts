import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatProgressBarModule} from '@angular/material';
import {ProgressBarComponent} from './progress-bar.component';

@NgModule({
  imports: [
    CommonModule,
    MatProgressBarModule
  ],
  declarations: [ ProgressBarComponent],
  exports: [ProgressBarComponent]
})
export class ProgressBarModule { }
