import { isNullOrUndefined } from 'util';

export class Utils {

    public static isObjectAndNotNull(obj): boolean {
        return typeof obj === 'object' && obj !== null;
    }

    public static buildRestApiURL(language: string, urlPiece: string, params?): string {
        console.log(params);
        if (!isNullOrUndefined(params)) {
            Object.keys(params).forEach((key) => {
                if (!isNullOrUndefined(params[key])) {
                    urlPiece = urlPiece.replace('{' + key + '}', params[key]);
                }else{
                    urlPiece = urlPiece.replace('/{' + key + '}', "");
                }
            });
        }
        return '/api/' + language + '/' + urlPiece;
    }

}
