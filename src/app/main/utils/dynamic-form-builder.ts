import {
    DynamicCheckboxModel,
    DynamicFormControlModel,
    DynamicFormGroupModel,
    DynamicInputModel,
    DynamicSelectModel,
    DynamicDatePickerModel
} from '@ng-dynamic-forms/core';
import { isNullOrUndefined } from 'util';
import { Utils } from './utils';

export class DynamicFormBuilder {

    public static builder(fields: any[]): DynamicFormControlModel[] {
        const dynamicFields = [];
        fields.forEach(field => {
            let dynamicField = null;
            if (field.custom) {
                dynamicField = this.customField(field);
            } else {
                dynamicField = this.generalField(field);
            }

            if (!isNullOrUndefined(dynamicField)) {
                dynamicFields.push(dynamicField);
            }
        });
        return dynamicFields;
    }

    public static getParsedFormResult(rawValues, result): void {
        if (!isNullOrUndefined(rawValues['passwordSetterGroup'])) {
            delete rawValues['passwordSetterGroup']['passwordRetype'];
        }
        Object.keys(rawValues).forEach(key => {
            const value = rawValues[key];
            if (value instanceof Date){
                result[key] = value.toISOString();
            }
            if (Utils.isObjectAndNotNull(value)) {
                DynamicFormBuilder.getParsedFormResult(value, result);
            } else {
                if (typeof value === 'boolean') {
                    result[key] = value ? 1 : 0;
                } else {
                    result[key] = value;
                }

            }
        })

    }

    private static customField(field: any): DynamicFormControlModel {
        switch (field.name) {
            case 'password':
                const password = new DynamicInputModel({
                    id: 'password',
                    label: field.name,
                    inputType: 'password',
                    validators: {
                        required: null,
                    }
                });
                const passwordConfirm = new DynamicInputModel({
                    id: 'passwordRetype',
                    label: 'Retype Password',
                    inputType: 'password',
                    validators: {
                        required: null,
                    }
                });
                return new DynamicFormGroupModel({
                    id: 'passwordSetterGroup',
                    group: [
                        password,
                        passwordConfirm,
                    ],
                    validators: { matchingPasswords: null },
                    errorMessages: {
                        noMatchingPasswords: 'Passwords need to match',
                    }
                });
            case 'contact_assignment':
                return new DynamicCheckboxModel({
                    id: field.name,
                    label: field.title,
                    value: !isNullOrUndefined(field.values) ? (!!field.values[0]) : false,
                    validators: { checkboxValidator: null },
                    errorMessages: {
                        notChecked: 'Field is required'
                    },
                });

            default:
                return this.generalField(field);
        }
    }

    private static generalField(field: any): DynamicFormControlModel {
        switch (field.type) {
            case 'date':
                return new DynamicDatePickerModel({
                    id: field.name,
                    value: !isNullOrUndefined(field.values) && !isNullOrUndefined(field.values[0]) ? new Date(field.values[0]) : '',
                    placeholder: field.title,
                    validators: field.require ? { required: null } : null,
                    errorMessages: {
                        required: 'Field is required'
                    }
                });
            case 'text':
                return new DynamicInputModel({
                    id: field.name,
                    value: !isNullOrUndefined(field.values) ? field.values[0] : '',
                    placeholder: field.title,
                    validators: field.require ? { required: null } : null,
                    errorMessages: {
                        required: 'Field is required'
                    }
                });
            case 'select':
                return new DynamicSelectModel<string>({
                    id: field.name,
                    placeholder: field.title,
                    value: !isNullOrUndefined(field.values) ? field.values[0] : '',
                    options: field.options,
                    validators: field.require ? { required: null } : null,
                    errorMessages: {
                        required: 'Field is required'
                    }
                });
            case 'file':
                return null;
        }
    }
}
