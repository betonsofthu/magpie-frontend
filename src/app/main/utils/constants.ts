export class Constants {

    public static USER_LAYOUT: string = 'user-layout-theme';
    public static COMPANY_LAYOUT: string = 'company-layout-theme';
    public static ADMIN_LAYOUT: string = 'admin-layout-theme';
}
