import {NgModule} from '@angular/core';
import {NotificationComponent} from './notification.component';
import {MatIconModule} from '@angular/material';
import {OverlayModule} from '@angular/cdk/overlay';
import {CommonModule} from '@angular/common';

@NgModule({
    imports: [CommonModule, MatIconModule, OverlayModule],
    declarations: [NotificationComponent],
    entryComponents: [NotificationComponent]
})
export class NotificationModule {
}
