import {TemplateRef} from '@angular/core';

export class NotificationData {

    type: NotificationType;
    message?: string;
    template?: TemplateRef<any>;
    templateContext?: {};


    constructor(type: NotificationType, message) {
        this.type = type;
        this.message = message;
    }

}

export type NotificationType = 'success' | 'warning' | 'error';
