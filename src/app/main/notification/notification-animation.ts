import {animate, AnimationTriggerMetadata, state, style, transition, trigger} from '@angular/animations';

export const notificationAnimations: {
    readonly fadeNotification: AnimationTriggerMetadata;
} = {
    fadeNotification: trigger('fadeAnimation', [
        state('in', style({opacity: 1})),
        transition('void => *', [style({opacity: 0}), animate('{{ fadeIn }}ms')]),
        transition(
            'default => closing',
            animate('{{ fadeOut }}ms', style({opacity: 0})),
        ),
    ]),
};

export type NotificationAnimationState = 'default' | 'closing';
