import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserProfileComponent} from './user-profile/user-profile.component';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, ReactiveFormsModule} from '@angular/forms';
import {DYNAMIC_VALIDATORS, DynamicFormsCoreModule, Validator, ValidatorFactory} from '@ng-dynamic-forms/core';
import {DynamicFormsMaterialUIModule} from '@ng-dynamic-forms/ui-material';
import {
    customAsyncFormGroupValidator,
    customDateRangeValidator,
    customForbiddenValidator,
    customValidator, matchingPasswords, checkboxValidator
} from './user-profile/validators';
import {MatButtonModule, MatNativeDateModule} from '@angular/material';
import {FlexLayoutModule} from '@angular/flex-layout';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
    imports: [
        FlexLayoutModule,
        CommonModule,
        ReactiveFormsModule,
        DynamicFormsCoreModule,
        DynamicFormsMaterialUIModule,
        MatNativeDateModule,
        MatButtonModule,
        TranslateModule
    ],
    declarations: [
        UserProfileComponent
    ],
    exports: [
        UserProfileComponent
    ],
    providers: [
        {
            provide: NG_VALIDATORS,
            useValue: checkboxValidator,
            multi: true
        },
        {
            provide: NG_VALIDATORS,
            useValue: customValidator,
            multi: true
        },
        {
            provide: NG_VALIDATORS,
            useValue: matchingPasswords,
            multi: true
        },
        {
            provide: NG_VALIDATORS,
            useValue: customDateRangeValidator,
            multi: true
        },
        {
            provide: NG_ASYNC_VALIDATORS,
            useValue: customAsyncFormGroupValidator,
            multi: true
        },
        {
            provide: DYNAMIC_VALIDATORS,
            useValue: new Map<string, Validator | ValidatorFactory>([
                ['matchingPasswords', matchingPasswords],
                ['checkboxValidator', checkboxValidator],
                ['customValidator', customValidator],
                ['customDateRangeValidator', customDateRangeValidator],
                ['customForbiddenValidator', customForbiddenValidator],
                ['customAsyncFormGroupValidator', customAsyncFormGroupValidator]
            ])
        }
    ]
})
export class UsersModule {
}
