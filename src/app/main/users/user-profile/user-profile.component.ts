import {Component, ElementRef, OnInit} from '@angular/core';
import {DynamicFormControlModel, DynamicFormLayout, DynamicFormService} from '@ng-dynamic-forms/core';
import {FormGroup} from '@angular/forms';
import {UserService} from '../../service/user/user.service';
import {DynamicFormBuilder} from '../../utils/dynamic-form-builder';
import {NotificationService} from '../../notification/notification.service';
import {HttpParams} from '@angular/common/http';
import {isNullOrUndefined} from 'util';
import {SharedService} from '../../service/shared.service';
import {CompaniesService} from '../../service/companies/companies.service';

@Component({
    selector: 'app-user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

    formModel: DynamicFormControlModel[] = null;
    formGroup: FormGroup;
    selectedLogo: File;
    formId: number;

    uploadedLogoSrc: string;

    constructor(private formService: DynamicFormService,
                private sharedService: SharedService,
                private userService: UserService,
                private compService: CompaniesService,
                private notifyService: NotificationService,
                private element: ElementRef) {
    }

    /*
        url = CompaniesService.GET_EDIT_COMPANY_FORM_FIELDS_URL;
        }

        this.sharedService.getFormFields(url, this.compId).subscribe(formFieldsResp => {
            this.formId = formFieldsResp.formId;
            this.formModel = DynamicFormBuilder.builder(formFieldsResp.fields);
            this.formGroup = this.formService.createFormGroup(this.formModel);
        });
     */

    ngOnInit() {
        let url = SharedService.GET_USER_FORM_FIELDS_URL;
        let params = null;
        if (this.sharedService.isCompanyLayout()) {
            url = CompaniesService.GET_EDIT_COMPANY_FORM_FIELDS_URL;
            params = {company: this.compService.company.id};
        }

        this.sharedService.getFormFields(url, params).subscribe(formFieldsResp => {
            this.formId = formFieldsResp.formId;
            const avatar = formFieldsResp.fields.find((field) => {
                return field.name === 'avatar';
            });
            if (!isNullOrUndefined(avatar) && !isNullOrUndefined(avatar['values'])) {
                this.uploadedLogoSrc = avatar['values'][0];
            }

            this.formModel = DynamicFormBuilder.builder(formFieldsResp.fields);
            this.formGroup = this.formService.createFormGroup(this.formModel);
        });
    }

    onBlur($event) {
        console.log(`Material blur event on: ${$event.model.id}: `, $event);
    }

    onChange($event) {
        console.log(`Material change event on: ${$event.model.id}: `, $event);
    }

    onFocus($event) {
        console.log(`Material focus event on: ${$event.model.id}: `, $event);
    }

    onMatEvent($event) {
        console.log(`Material ${$event.type} event on: ${$event.model.id}: `, $event);
    }

    logoImgChangeListner($event: Event) {
        const reader = new FileReader();
        const image = this.element.nativeElement.querySelector('.img');
        const file = (<HTMLInputElement>event.target).files[0];
        reader.onloadend = function (e) {
            const src = reader.result;
            image.src = src;
        };
        if (file && file.type.match('image.*')) {
            reader.readAsDataURL(file);
            this.selectedLogo = file;
        }
    }

    saveProfile() {
        const values = {form: this.formId};
        DynamicFormBuilder.getParsedFormResult(this.formGroup.getRawValue(), values);
        const formData = new FormData();
        if (!isNullOrUndefined(this.selectedLogo)) {
            formData.append('avatar', this.selectedLogo, this.selectedLogo.name);
        }

        Object.keys(values).forEach(key => {
            formData.append(key, values[key]);
        });

        let url = SharedService.SAVE_USER_PROFILE_URL
        let params = null;
        if (this.sharedService.isCompanyLayout()) {
            url = CompaniesService.SAVE_EDITED_COMPANY_URL;
            params = {comapny: this.compService.company.id};
        }

        this.sharedService.saveForm(url, formData, params).subscribe(() => {
            this.notifyService.success('Save user profile was successful');
            this.userService.getUserDetailsForce().subscribe();
        });
    }
}
