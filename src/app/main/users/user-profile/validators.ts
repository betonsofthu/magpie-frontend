import {AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn} from '@angular/forms';

export function customValidator(control: AbstractControl): ValidationErrors | null {

    let hasError = control.value ? (control.value as string).startsWith('abc') : false;

    return hasError ? {customValidator: true} : null;
}

export function customDateRangeValidator(group: FormGroup): ValidationErrors | null {

    let dateArrival = group.get('arrivalDate').value as Date,
        dateDeparture = group.get('departureDate').value as Date,
        hasError = false;

    if (dateArrival && dateDeparture) {
        hasError = dateArrival >= dateDeparture || dateDeparture <= dateArrival;
    }

    return hasError ? {customDateRangeValidator: true} : null;
}

export function customForbiddenValidator(forbiddenValue: string): ValidatorFn {

    return (control: AbstractControl): ValidationErrors | null => {

        if (control && control.value === forbiddenValue) {
            return {forbidden: true};
        }

        return null;
    }
}

export function customAsyncFormGroupValidator(formGroup: FormGroup): Promise<ValidationErrors | null> {

    return new Promise((resolve, reject) => {
        console.log('async validation');
        resolve(null);
    });
}

export function matchingPasswords(control: FormControl): {[s: string]: boolean} {
    const password = control.get('password');
    const confirmPassword = control.get('passwordRetype');
    if (password.value !== confirmPassword.value) {
        console.log('passwords do NOT match');
        return {
            'noMatchingPasswords': true
        };
    } else {
        console.log('passwords match');
        return null;
    }
}


export function checkboxValidator(control: AbstractControl): ValidationErrors | null {
    return !control.value ? {notChecked: true} : null;
}
