import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyEventsComponent } from './my-events/my-events.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule, MatCardModule, MatIconModule, MatButtonModule, MatTableModule, MatMenuModule, MatSelectModule, MatExpansionModule, MatStepperModule, MatPaginatorModule, MatCheckboxModule, MatMenu } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { AddEditEventComponent } from './add-edit-event/add-edit-event.component';
import { DynamicFormsMaterialUIModule } from '@ng-dynamic-forms/ui-material';
import { DynamicFormsCoreModule } from '@ng-dynamic-forms/core';
import { EventsComponent } from './events.component';
import { ClientEventsComponent } from './client-events.component';
import { CompanyEventsComponent } from './company-events.component';
import { EventDetailsComponent } from './event-details/event-details.component';
import { EventProductsComponent } from './event-details/event-products.component';
import { ProductPageComponent } from './event-details/product-page/product-page.component';
import { EventRegisterDialogComponent } from './event-details/event-register-dialog.component';
import { EventRegisterService } from '../service/event/event-register.service';


@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    TranslateModule,
    MatMenuModule,
    MatSelectModule,
    MatExpansionModule,
    RouterModule,
    MatStepperModule,
    MatPaginatorModule,
    MatCheckboxModule,
    SharedModule,
    DynamicFormsMaterialUIModule,
    ReactiveFormsModule,
    DynamicFormsCoreModule,
    MatMenuModule,
    MatExpansionModule,
  ],
  declarations: [
    MyEventsComponent,
    AddEditEventComponent,
    EventsComponent,
    ClientEventsComponent,
    CompanyEventsComponent,
    EventProductsComponent,
    EventDetailsComponent,
    ProductPageComponent,
    EventRegisterDialogComponent
  ],
  exports: [
    MyEventsComponent,
    AddEditEventComponent,
    EventsComponent,
    ProductPageComponent,
    EventRegisterDialogComponent,
    EventProductsComponent
  ],
  providers: [
    FormBuilder,
    EventRegisterService
  ],
  entryComponents: [
    AddEditEventComponent,
    EventRegisterDialogComponent
  ]
})
export class EventsModule { }
