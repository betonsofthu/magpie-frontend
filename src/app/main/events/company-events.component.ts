import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EventService } from '../service/event/event.service';

@Component({
  selector: 'app-company-events',
  templateUrl: './company-events.component.html',
  styleUrls: ['./company-events.component.scss']
})
export class CompanyEventsComponent implements OnInit {

  companyId: number;
  companyDetails: any;
  events: any[];

  constructor(private router: ActivatedRoute,
              private eventService: EventService) { }

  ngOnInit() {
    this.router.queryParams.subscribe((params) => {
      this.eventService.getCompanyEvents(params['company']).subscribe((resp) => {
          this.events = resp.events;
          delete resp.events;
          this.companyDetails = resp;
      });
    });
  }

}
