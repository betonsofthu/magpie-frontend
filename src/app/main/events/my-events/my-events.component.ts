import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { isNullOrUndefined } from 'util';
import { AddEditEventComponent } from '../add-edit-event/add-edit-event.component';
import { EventService } from '../../service/event/event.service';

@Component({
  selector: 'app-my-events',
  templateUrl: './my-events.component.html',
  styleUrls: ['./my-events.component.scss']
})
export class MyEventsComponent implements OnInit {

  events: any[] = [];
  displayedColumns: string[] = ['id', 'title', 'created_at', 'menu'];

  constructor(private matDialog: MatDialog,
    private eventService: EventService) { }

  ngOnInit() {
    this.init();
  }

  init() {
    this.eventService.getEvents().subscribe((events) => {
      this.events = events;
    });
  }

  addEditEvent(eventId?: number) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = '80%';
    dialogConfig.height = '80%';
    dialogConfig.panelClass = 'custom-modal-container';
    if (!isNullOrUndefined(eventId)) {
      dialogConfig.data = eventId;
    }

    const dialogRef = this.matDialog.open(AddEditEventComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      this.init();
    });
  }

  clickEvent(event): void {
    this.addEditEvent(event.id);
  }

  deleteEvent(id): void {
    this.eventService.deleteEvent(id).subscribe(() => {
      this.init();
    });
  }

}
