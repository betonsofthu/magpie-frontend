import { Component, OnInit } from '@angular/core';
import { EventService } from '../service/event/event.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-client-events',
  templateUrl: './client-events.component.html',
  styleUrls: ['./client-events.component.scss']
})
export class ClientEventsComponent implements OnInit {

  // allEvents: Observable<any[]>;
  allEvents: any[];

  constructor(public eventService: EventService) { }

  ngOnInit() {
    this.eventService.getAllClientEvents().subscribe((resp) =>{
      this.allEvents = resp;
    });
  }



}
