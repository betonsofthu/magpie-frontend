import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { EventService } from 'app/main/service/event/event.service';
import { DynamicFormControlModel, DynamicFormService } from '@ng-dynamic-forms/core';
import { FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatHorizontalStepper, MatDialogRef } from '@angular/material';
import { DynamicFormBuilder } from 'app/main/utils/dynamic-form-builder';
import { isNullOrUndefined } from 'util';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-add-edit-event',
  templateUrl: './add-edit-event.component.html',
  styleUrls: ['./add-edit-event.component.scss']
})
export class AddEditEventComponent implements OnInit {

  @ViewChild('stepper') stepper: MatHorizontalStepper;
  @ViewChild('eventImg') eventImgElement: ElementRef;

  formModelBasic: DynamicFormControlModel[] = null;
  formGroupBasic: FormGroup;
  formIdBasic: number;

  formModelSpecific: DynamicFormControlModel[] = null;
  formGroupSpecific: FormGroup;
  formIdSpecific: number;

  formModelSelectedType: DynamicFormControlModel[] = null;
  formGroupSelectedType: FormGroup;
  formIdSelectedType: number;

  formModelInfo: DynamicFormControlModel[] = null;
  formGroupInfo: FormGroup;
  formIdInfo: number;

  isLinear = true;
  selectedEventType: number;

  eventImg: File;
  eventImgPath: string;

  basicFields: any[] = [];

  constructor(private eventService: EventService,
    private formService: DynamicFormService,
    private dialogRef: MatDialogRef<AddEditEventComponent>,
    @Inject(MAT_DIALOG_DATA) private eventId?: number) { }

  ngOnInit() {
    this.initBasicFields().subscribe(() => {
      if (!isNullOrUndefined(this.eventId) && this.eventId > 0){
        this.initSpecificFields();
        this.initEventSelectedTypeFields(this.selectedEventType);
        this.initEventInfo();
        this.isLinear = false;
      }
    });
    
  }

  imgChangeListener($event: Event) {

    const reader = new FileReader();
    const file = (<HTMLInputElement>event.target).files[0];
    const image = this.eventImgElement.nativeElement;
    reader.onloadend = function (e) {
      const src = reader.result;
      image.src = src;
    };
    if (file && file.type.match('image.*')) {
      reader.readAsDataURL(file);
      this.eventImg = file;
    }
  }

  stepChanged($event) {
    const index = $event.selectedIndex;
    this.initEventStepFields(index);
  }

  initEventStepFields(index: number){
    switch (index) {
      case 1:
        if (isNullOrUndefined(this.formModelSpecific)) {
          this.initSpecificFields();
        }
        break;
      case 2:
       
        if (!isNullOrUndefined(this.selectedEventType)) {
          this.initEventSelectedTypeFields(this.selectedEventType);
        }
        break;
      case 3:
        if (isNullOrUndefined(this.formModelInfo)) {
          this.initEventInfo();
        }
        break;
    }
  }

  initBasicFields(): Observable<any> {
    return this.eventService.getBasicFields(this.eventId).pipe(tap((eventFields) => {
      this.formIdBasic = eventFields.formId;
      eventFields.fields.forEach(field => {
        if (field.name === 'event_image' && !isNullOrUndefined(field.values)) {
          this.eventImgPath = field.values[0];
        }
        if (field.name === 'event_category' && !isNullOrUndefined(field.values)) {
          this.selectedEventType = field.values[0];
        }
      });
      this.formModelBasic = DynamicFormBuilder.builder(eventFields.fields);
      this.formGroupBasic = this.formService.createFormGroup(this.formModelBasic);
    }));
  }

  initSpecificFields(): void {
    this.eventService.getSpecificFields(this.eventId).subscribe((eventFields) => {
      this.formIdSpecific = eventFields.formId;
      this.formModelSpecific = DynamicFormBuilder.builder(eventFields.fields);
      this.formGroupSpecific = this.formService.createFormGroup(this.formModelSpecific);
    });
  }


  initEventSelectedTypeFields(type: number): void {
    let obs = null;
    this.formModelSelectedType = null;
    this.formGroupSelectedType = null;
    switch (type) {
      case 0:
      case 1:
        this.initEventStepFields(3);
        return;
      case 2:
        obs = this.eventService.getEventAuction(this.eventId);
        break;
      case 3:
        obs = this.eventService.getEventShop(this.eventId);
        break;
    }

    obs.subscribe((eventFields) => {
      this.formIdSelectedType = eventFields.formId;
      this.formModelSelectedType = DynamicFormBuilder.builder(eventFields.fields);
      this.formGroupSelectedType = this.formService.createFormGroup(this.formModelSelectedType);
    });
  }

  initEventInfo(): void {
    this.eventService.getEventInfo(this.eventId).subscribe((eventFields) => {
      this.formIdInfo = eventFields.formId;
      this.formModelInfo = DynamicFormBuilder.builder(eventFields.fields);
      this.formGroupInfo = this.formService.createFormGroup(this.formModelInfo);
    });
  }

  saveFields(formId: number, formGroup: FormGroup, eventStepNum: number): void {
    if (isNullOrUndefined(formGroup)) {
      this.stepper.next();
      return;
    }
    let values = null;
    values = { form: formId };
    DynamicFormBuilder.getParsedFormResult(formGroup.getRawValue(), values);
    
    if (eventStepNum === 1) {
      this.selectedEventType = values['event_category'];
      if (!isNullOrUndefined(this.eventImg)){
        const formData = new FormData();

        if (!isNullOrUndefined(this.eventImg)) {
          formData.append('event_image', this.eventImg, this.eventImg.name);
        }
        
        Object.keys(values).forEach(key => {
          formData.append(key, values[key]);
        });
        values = formData;
      }
    }
    this.eventService.saveFields(values, this.eventId).subscribe((eventId) => {
      this.eventId = eventId;
      switch (eventStepNum) {
        case 5:
          this.dialogRef.close(true);
          return;
        default:
          break;
      }
      this.stepper.next();
    }, (error) => {
      this.selectedEventType = null;
    });
  }



}
