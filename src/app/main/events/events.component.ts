import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { isNullOrUndefined } from 'util';
import { EventService } from '../service/event/event.service';
import { SharedService } from '../service/shared.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {

  @Input() allEvents = true;
  @Input() events: any[];
  @Output() eventChanged = new EventEmitter<any>();


  constructor(private sharedService: SharedService) { }

  ngOnInit() {
    console.log(this.events);
  }

  navigateEventDetails(): void {

  }

  addRemoveFavorite(event): void {
    if (!isNullOrUndefined(event.user_favorite) && event.user_favorite === true) {
      this.sharedService.removeFromFavorite(event.id, 'event').subscribe((value) => {
        event.user_favorite = false;
        this.eventChanged.emit(event);

      });
    } else {
      this.sharedService.addToFavorite(event.id, 'event').subscribe((value) => {
        event.user_favorite = true;
        this.eventChanged.emit(event);

      });
    }
  }

  isFavorite(event): boolean {
    return !isNullOrUndefined(event.user_favorite) && event.user_favorite === true;
  }

}
