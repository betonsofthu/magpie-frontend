import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { EventRegisterService } from 'app/main/service/event/event-register.service';

@Component({
  selector: 'app-event-register-dialog',
  templateUrl: './event-register-dialog.component.html',
  styleUrls: ['./event-register-dialog.component.scss']
})
export class EventRegisterDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<EventRegisterDialogComponent>,
                 @Inject(MAT_DIALOG_DATA) public eventDetails: any) { }

  ngOnInit() {
  }

}
