import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { isNullOrUndefined } from 'util';
import { Router } from '@angular/router';
import { SharedService } from 'app/main/service/shared.service';

@Component({
  selector: 'app-event-products',
  templateUrl: './event-products.component.html',
  styleUrls: ['./event-products.component.scss']
})
export class EventProductsComponent implements OnInit {

  @Input() products: any[];
  @Input() eventId: number;
  @Input() isFavoriteList: boolean = false;
  @Output() productChanged = new EventEmitter<any>();

  constructor(private router: Router,
    private sharedService: SharedService) { }

  ngOnInit() {

  }

  openProductDetails(product) {
    let eventIdParam = this.eventId;
    if (this.isFavoriteList) {
      eventIdParam = product.relation_id;
    }
    this.router.navigate(['/product-page'], {
      queryParams: {
        productId: product.id,
        eventId: eventIdParam
      }
    })
    // const dialogConf = new MatDialogConfig();
    // dialogConf.autoFocus = true;
    // dialogConf.width = '700px';
    // dialogConf.height = 'auto';
    // dialogConf.data = {productId: productId, eventCategory: this.eventCategory};
    // dialogConf.panelClass = 'custom-modal-container';

    // const dialogRef = this.matDialog.open(ProductPageComponent, dialogConf);
    // dialogRef.afterClosed().subscribe(result => {

    // });
  }

  addRemoveFavorite(product): void {

    if (!isNullOrUndefined(product.user_favorite) && product.user_favorite === true) {
      this.sharedService.removeFromFavorite(product.id, 'product', this.eventId).subscribe((value) => {
        product.user_favorite = false;
        this.productChanged.emit(product);

      });
    } else {
      this.sharedService.addToFavorite(product.id, 'product', this.eventId).subscribe((value) => {
        product.user_favorite = true;
        this.productChanged.emit(product);

      });
    }
  }

  isFavorite(product): boolean {
    return !isNullOrUndefined(product.user_favorite) && product.user_favorite === true;
  }

}
