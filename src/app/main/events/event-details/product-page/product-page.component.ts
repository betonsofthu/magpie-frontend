import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CatalogService } from 'app/main/service/catalog/catalog.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { EventRegisterService } from 'app/main/service/event/event-register.service';

@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.scss']
})
export class ProductPageComponent implements OnInit {

  productId: number;
  eventId: number;
  productDetails: any;
  eventDetails: any;
  images: any[] = [];

  constructor(
              private router: ActivatedRoute,
              private catalogService: CatalogService,
              private eventRegister: EventRegisterService
              ) { 
              
              }

  ngOnInit() {
    this.router.queryParams.subscribe((params) => {
      this.productId = params['productId'];
      this.eventId =  params['eventId'];
      this.getProductDetails(this.productId, this.eventId);
    });
    
  }

  getProductDetails(prodId: number, eventId: number){
    this.catalogService.getProductDetails(prodId, eventId).subscribe((resp) => {
      this.productDetails = resp;
      this.eventDetails = resp.event;
      this.images= this.productDetails.images;
    });
  }

  getMainImage(){
    if (this.images.length > 0){
      return this.images[0];
    }
    return "";
  }

  getImages(){
    return this.images;
  }

  isAuction(){
    return this.eventDetails.event_category == 2;
  }

  isShop(){
    return this.eventDetails.event_category == 3;
  }

  isNotRegistered(){
    return this.eventDetails.event_sharing_option == 0 && !this.eventRegister.isRegistered;
  }

}
