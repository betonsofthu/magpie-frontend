import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { EventService } from 'app/main/service/event/event.service';
import { ActivatedRoute } from '@angular/router';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { EventRegisterService } from 'app/main/service/event/event-register.service';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { fromEvent } from 'rxjs';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.scss']
})
export class EventDetailsComponent implements OnInit, AfterViewInit {


  @ViewChild('searchBox') searchInput: ElementRef;
  eventDetails: any;
  products: any[];
  categories: string[] = [];

  pageIndex = 0;
  pageSize = 10;
  lastPage = 50;
  lastSearch = null;
  lastCategory = null;

  eventId = -1;

  constructor(private router: ActivatedRoute,
    private eventService: EventService,
    public eventRegister: EventRegisterService) { }

  ngOnInit() {
    this.router.queryParams.subscribe((params) => {
      this.eventId = params['event'];
      this.getProducts(1, null, null, true);
      this.eventRegister.checkIfUserRegistered(this.eventId);
    });
  }

  ngAfterViewInit(): void {

  }

  getProducts(page = 1, search = null, categories: any[] = null, getDetails = false) {
    let categoriesStr = null;
    if (!isNullOrUndefined(categories)){
      categoriesStr = categories.join(",");
    }
    this.eventService.getEventDetails(this.eventId, page, search, categoriesStr).subscribe((resp) => {
      this.products = resp.catalog.data;
      this.lastPage = resp.catalog.last_page;
      if (getDetails) {
        this.categories = resp.categories;
        delete resp.catalog;
        delete resp.categories;
        this.eventDetails = resp;
        this.initSearchChangesObservable();
      }
     
    });
  }

  initSearchChangesObservable() {
    setTimeout(() => {
      fromEvent(this.searchInput.nativeElement, 'keyup')
            .pipe(
                debounceTime(200),
                distinctUntilChanged(),
            )
            .subscribe( (event: KeyboardEvent) => {
              const value = (event.target as HTMLInputElement).value;
              this.lastSearch = value;
              this.getProducts(1, value, this.lastCategory);
            });
    }, 0);

  }

  pagination(page) {
    this.getProducts(page);
  }

  categoriesChanged(event){
    this.lastCategory = event.value;
    this.getProducts(1, this.lastSearch, event.value);
  }

}
