import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductImageViewerComponent } from './product-image-viewer/product-image-viewer.component';
import { MatIconModule, MatButtonModule, MatSelectModule, MatInputModule, MatFormFieldModule } from '@angular/material';
import { PaginationComponent } from './pagination/pagination.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
    FlexLayoutModule,
    MatSelectModule,
    MatInputModule,
    FormsModule
  ],
  declarations: [ProductImageViewerComponent,
    PaginationComponent],
  exports: [ProductImageViewerComponent, PaginationComponent]
})
export class SharedModule { }
