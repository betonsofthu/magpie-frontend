import {Component, HostListener, Input} from '@angular/core';

@Component({
    selector: 'app-product-image-viewer',
    templateUrl: './product-image-viewer.component.html',
    styleUrls: ['./product-image-viewer.component.scss']
})
export class ProductImageViewerComponent {

    @Input() currentIndex: number;
    @Input() imageUrls: (string | ArrayBuffer)[] = [];

    public isVisible = false;

    constructor() {
    }

    @HostListener('click', ['$event'])
    listenToMouseClick(event): void {
        if (event.target.id === 'main-container') {
            this.isVisible = false;
        }
    }

    public open(index: number, images: (string | ArrayBuffer)[] ): void {
        this.isVisible = true;
        this.currentIndex = index;
        this.imageUrls = images;
    }

    public next(): void {
        if (this.currentIndex < (this.imageUrls.length - 1)) {
            this.currentIndex = this.currentIndex + 1;
        }
    }

    public previous(): void {
        if (this.currentIndex > 0) {
            this.currentIndex = this.currentIndex - 1;
        }
    }

    public hasNextItem(): boolean {
        return this.currentIndex < (this.imageUrls.length - 1);
    }

    public hasPreviousItem(): boolean {
        return this.currentIndex > 0;
    }

}
