import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

    @Input() maxRange = 10;
    @Output() selectedIndexCallback = new EventEmitter<number>();
    private minRange = 1;
    private _selectedIndex = this.minRange;
    pageNums = [];

    set selectedIndex(value: number) {
        this._selectedIndex = value;
        this.selectedIndexCallback.emit(value);
    }

    get selectedIndex(): number {
        return this._selectedIndex;
    }

    constructor() {
    }

    ngOnInit(): void {
        for (let index = 1; index <= this.maxRange; index++) {
            this.pageNums.push(index);
        }
    }

    minus(): void {
        if (this.selectedIndex > this.minRange) {
            this.selectedIndex--;
        }
    }

    plus(): void {
        if (this.selectedIndex < this.maxRange) {
            this.selectedIndex++;
        }
    }

    onChange(value): void {
        this.selectedIndex = value;
        this.selectedIndexCallback.emit(value);
    }


}
