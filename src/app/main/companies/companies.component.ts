import {Component, OnInit} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {AddCompaniesModalComponent} from './add-companies-modal.component';
import {magpieAnimations} from '../../animations';
import {SharedService} from '../service/shared.service';
import {Constants} from '../utils/constants';
import {CompaniesService} from '../service/companies/companies.service';
import {Company} from '../service/companies/company';
import {isNullOrUndefined} from 'util';


@Component({
    selector: 'app-companies',
    templateUrl: './companies.component.html',
    styleUrls: ['./companies.component.scss'],
    animations: magpieAnimations
})


export class CompaniesComponent implements OnInit {

    displayedColumns: string[] = ['id', 'name', 'registrationNumber', 'menu'];
    companies: Company[] = [];

    constructor(private matDialg: MatDialog, private sharedService: SharedService, private compService: CompaniesService) {

    }

    ngOnInit() {
        this.initCompanies();
    }

    initCompanies() {
        this.compService.getUserCompanies().subscribe(companies => {
            this.companies = companies;
        });
    }

    addEditCompany(compId?) {
        const dialogConf = new MatDialogConfig();
        dialogConf.autoFocus = true;
        dialogConf.width = '600px';
        dialogConf.maxHeight = '500px';
        dialogConf.panelClass = 'custom-modal-container';
        if (!isNullOrUndefined(compId)) {
            dialogConf.data = compId;
        }

        const dialogRef = this.matDialg.open(AddCompaniesModalComponent, dialogConf);
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.initCompanies();
            }
        });
    }

    clickCompany(company: Company) {
        this.compService.getCompanyDetails(company.id).subscribe((compDetails) => {
                company.avatar = compDetails.avatar;
                this.sharedService.changeLayout(Constants.COMPANY_LAYOUT);
                this.compService.company = company;
                
        });
      
    }


}
