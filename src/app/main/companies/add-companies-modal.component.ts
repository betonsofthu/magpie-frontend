import {Component, ElementRef, Inject, OnInit} from '@angular/core';
import {DynamicFormControlModel, DynamicFormService} from '@ng-dynamic-forms/core';
import {FormGroup} from '@angular/forms';
import {DynamicFormBuilder} from '../utils/dynamic-form-builder';
import {NotificationService} from '../notification/notification.service';
import {SharedService} from '../service/shared.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {RegisterComponent} from '../authentication/register/register.component';
import {CompaniesService} from '../service/companies/companies.service';
import {isNullOrUndefined} from 'util';
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'app-add-companies-modal',
    templateUrl: './add-companies-modal.component.html',
    styleUrls: ['./add-companies-modal.component.scss']
})
export class AddCompaniesModalComponent implements OnInit {

    formModel: DynamicFormControlModel[] = null;
    formGroup: FormGroup;
    formId: number;
    title = 'companies.add';

    constructor(private formService: DynamicFormService,
                private notifyService: NotificationService,
                private sharedService: SharedService,
                private dialogRef: MatDialogRef<AddCompaniesModalComponent>,
                private transService: TranslateService,
                @Inject(MAT_DIALOG_DATA) private compId?: number) {
    }

    ngOnInit() {
        let url = CompaniesService.GET_ADD_COMPANY_FORM_FIELDS_URL;
        if (!isNullOrUndefined(this.compId)) {
            this.title = 'companies.edit';
            url = CompaniesService.GET_EDIT_COMPANY_FORM_FIELDS_URL;
        }

        this.sharedService.getFormFields(url, {company:this.compId}).subscribe(formFieldsResp => {
            this.formId = formFieldsResp.formId;
            this.formModel = DynamicFormBuilder.builder(formFieldsResp.fields);
            this.formGroup = this.formService.createFormGroup(this.formModel);
        });
    }

    saveCompany() {
        const values = {form: this.formId};
        DynamicFormBuilder.getParsedFormResult(this.formGroup.getRawValue(), values);
        let saveUrl = CompaniesService.SAVE_COMPANY_URL;
        if (!isNullOrUndefined(this.compId)) {
            saveUrl = CompaniesService.SAVE_EDITED_COMPANY_URL;
        }
        this.sharedService.saveForm(saveUrl, values, {company:this.compId}).subscribe(() => {
            this.notifyService.success(this.transService.instant('companies.success_save'));
            this.dialogRef.close(true);
        });
    }

    close() {
        this.dialogRef.close(false);
    }
}
