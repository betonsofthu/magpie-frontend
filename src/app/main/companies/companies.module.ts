import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CompaniesComponent} from './companies.component';
import {MatButtonModule, MatFormFieldModule, MatIconModule, MatMenuModule, MatTableModule} from '@angular/material';
import {AddCompaniesModalComponent} from './add-companies-modal.component';
import {DynamicFormsMaterialUIModule} from '@ng-dynamic-forms/ui-material';
import {FlexLayoutModule} from '@angular/flex-layout';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TranslateModule} from '@ngx-translate/core';

import {DYNAMIC_VALIDATORS, DynamicFormsCoreModule, Validator, ValidatorFactory} from '@ng-dynamic-forms/core';
import { customValidator, matchingPasswords, customDateRangeValidator, customAsyncFormGroupValidator, checkboxValidator } from '../users/user-profile/validators';
import { FormsModule, ReactiveFormsModule, NG_VALIDATORS, FormBuilder } from '@angular/forms';
@NgModule({
    imports: [
        FlexLayoutModule,
        MatTableModule,
        CommonModule,
        FormsModule,
        MatFormFieldModule,
        MatIconModule,
        MatButtonModule,
        DynamicFormsMaterialUIModule,
        ReactiveFormsModule,
        DynamicFormsCoreModule,
        TranslateModule,
        MatMenuModule
  
    ],
    declarations: [
        CompaniesComponent,
        AddCompaniesModalComponent
    ],
    exports: [
        CompaniesComponent,
        AddCompaniesModalComponent
    ],
    entryComponents: [
        AddCompaniesModalComponent
    ],
    providers: [
        FormBuilder
    ]
})
export class CompaniesModule {
}
