import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FavoritesComponent } from './favorites.component';
import { EventsModule } from '../events/events.module';
import { SharedModule } from '../shared/shared.module';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  imports: [
    FlexLayoutModule,
    CommonModule,
    EventsModule,
    SharedModule
  ],
  declarations: [FavoritesComponent],
  exports:[FavoritesComponent]
})
export class FavoritesModule { }
