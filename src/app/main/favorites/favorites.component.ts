import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SharedService } from '../service/shared.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent implements OnInit {

  type: string;
  items: any[] = null;
  lastPage: number;

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private sharedService: SharedService) { }

  ngOnInit() {
    this.type = this.activatedRoute.snapshot.url[1].path;
    this.getFavoriteItems();
  }

  getFavoriteItems(page = 1){
    this.sharedService.getFavorites(this.type, page).subscribe((resp)=>{
        this.items = resp.data.map((item) => {
          return item.detail;
        });
        this.lastPage = resp.last_page;
    });
  }

  
  changedItem(event): void{
    this.getFavoriteItems();
  }


  pagination(page) {
    this.getFavoriteItems(page);
  }

}
