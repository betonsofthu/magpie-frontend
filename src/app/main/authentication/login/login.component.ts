import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService, Credentials} from '../../service/authentication/authentication.service';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {RegisterComponent} from '../register/register.component';
import {UserService} from '../../service/user/user.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    credentials: Credentials;
    disableLoginBtn: boolean = false;

    google_login_url = AuthenticationService.LOGIN_WITH_GOOGLE;

    constructor(private router: Router, private authService: AuthenticationService, private userService: UserService, private matDialg: MatDialog) {
        this.credentials = new Credentials();
    }

    ngOnInit() {
    }

    onLogin() {
        this.disableLoginBtn = true;
        this.authService.login(this.credentials).subscribe(() => {
            this.userService.getUserDetails(true).subscribe(value => {
                this.router.navigate(['/dashboard']);
                this.disableLoginBtn = false;
            }, error => {
                this.disableLoginBtn = false;
            });

        }, error => {
            this.disableLoginBtn = false;
        });
    }

    signUp() {
        if (this.disableLoginBtn) {
            return;
        }
        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = true;
        dialogConfig.width = '400px';
        dialogConfig.maxHeight = '90%';
        dialogConfig.panelClass = 'custom-modal-container';
        const dialogRef = this.matDialg.open(RegisterComponent, dialogConfig);
        dialogRef.afterClosed().subscribe(result => {
            if (result === false){
                return;
            }
            this.credentials.user = result.email;
            this.credentials.password = result.password;
            this.onLogin();
        });
    }

    handleLoginResult(): void {
        this.userService.getUserDetails(true).subscribe(value => {
            this.router.navigate(['/dashboard']);
            this.disableLoginBtn = false;
        }, error => {
            this.disableLoginBtn = false;
        });

    }

    loginWithSocial(url) {
        this.authService.loginWithSocial(url).subscribe(()=>{
            this.handleLoginResult();
        });
    }
}
