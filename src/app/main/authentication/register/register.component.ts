import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {AuthenticationService} from '../../service/authentication/authentication.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {NotificationService} from '../../notification/notification.service';
import {MatDialogRef} from '@angular/material';
import { LangService } from 'app/main/service/lang/lang.service';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    registerForm: FormGroup;

    private _unsubscribeAll: Subject<any>;
    langs: any[] = [];

    constructor(private formBuilder: FormBuilder,
                private auth: AuthenticationService,
                private dialogRef: MatDialogRef<RegisterComponent>,
                private notificationService: NotificationService,
                private langService: LangService) {
        this._unsubscribeAll = new Subject();
    }

    ngOnInit() {
        this.langService.getLangs().subscribe((langs) => {
            console.log(langs);
            this.langs = langs;
        });
        this.registerForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required],
            passwordConfirm: ['', [Validators.required,  confirmPasswordValidator]],
            lang: ['', Validators.required],
            terms: [false, [Validators.required]]
        });

        this.registerForm.get('password').valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.registerForm.get('passwordConfirm').updateValueAndValidity();
            });
    }

    register() {
        const generalInfo = this.registerForm.value;
        if (this.registerForm.valid) {

            this.auth.register(generalInfo.firstName,generalInfo.lastName,generalInfo.email, generalInfo.lang, generalInfo.password).subscribe((res) => {
                this.dialogRef.close({email: generalInfo.email, password: generalInfo.password});
            });
        }
    }

    close() {
        this.dialogRef.close(false);
    }
}

export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if (!control.parent || !control) {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if (!password || !passwordConfirm) {
        return null;
    }

    if (passwordConfirm.value === '') {
        return null;
    }

    if (password.value === passwordConfirm.value) {
        return null;
    }

    return {'passwordsNotMatching': true};
};
