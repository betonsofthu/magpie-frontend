import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
    MatButtonModule,
    MatCheckboxModule,
    MatDialogModule,
    MatDividerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule
} from '@angular/material';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';

@NgModule({
    imports: [
        CommonModule,
        FlexLayoutModule,
        FormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatCheckboxModule,
        MatButtonModule,
        MatDialogModule,
        MatDividerModule,
        MatIconModule,
        ReactiveFormsModule,
        MatSelectModule
    ],
    declarations: [
        LoginComponent,
        RegisterComponent],
    providers: [
        FormBuilder
    ],
    exports: [
        LoginComponent,
        RegisterComponent
    ]
})
export class AuthenticationModule {
}
