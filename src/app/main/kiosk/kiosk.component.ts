import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../service/user/user';
import { UserService } from '../service/user/user.service';
import { AdminService } from '../service/user/admin.service';
import { AuthenticationService } from '../service/authentication/authentication.service';
import { SharedService } from '../service/shared.service';
import { Constants } from '../utils/constants';
import { PageEvent, MatPaginator, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-kiosk',
  templateUrl: './kiosk.component.html',
  styleUrls: ['./kiosk.component.scss']
})
export class KioskComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = ['id', 'name', 'email', 'created_at'];
  users: any[] = [];
  dataSource = new MatTableDataSource<any>([]);

  pageSize = 10;

  constructor(private userService: UserService,
             private sharedService: SharedService,
             private adminService: AdminService,
             private authService: AuthenticationService) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.getUsers();
  }

  getUsers(page = 1){
    this.userService.getAllUsers(page).subscribe((users) => {
      this.users = users.data;
      this.paginator.length = users.total;
      this.paginator.pageIndex = users.current_page - 1;
      this.dataSource = new MatTableDataSource<any>(this.users);
    });
  }

  goToUser(user){
    this.adminService.loginToUser(user.id).subscribe((resp) =>{
        this.authService.setAccessToken(resp.token);
        this.sharedService.changeLayout(Constants.USER_LAYOUT);
        this.userService.getUserDetails(true).subscribe();
    });
  }

  paginatorChanged(event: PageEvent) {
    this.getUsers(event.pageIndex + 1);
  }
}
