import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KioskComponent } from './kiosk.component';
import { MatTable, MatTableModule, MatCardModule, MatPaginatorModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  imports: [
    CommonModule,
    MatTableModule,
    MatCardModule,
    MatPaginatorModule
  ],
  declarations: [KioskComponent],
  exports: [KioskComponent]
})
export class KioskModule { }
