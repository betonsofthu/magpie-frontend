import { Injectable } from '@angular/core';
import { RestService } from '../rest/rest.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LangService {

public static GET_LANGS = '/api/langs/getAll'

  constructor(private restService: RestService) { }

  public getLangs(): Observable<any> {
    return this.restService.get(LangService.GET_LANGS).pipe(map( res => { 
       return res.getData();
    }));
  }

}
