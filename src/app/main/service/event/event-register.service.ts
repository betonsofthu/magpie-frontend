import { Injectable } from '@angular/core';
import { RestService } from '../rest/rest.service';
import { UserService } from '../user/user.service';
import { Utils } from 'app/main/utils/utils';
import { EventService } from './event.service';
import { SharedService } from '../shared.service';
import { map } from 'rxjs/operators';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { EventRegisterDialogComponent } from 'app/main/events/event-details/event-register-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class EventRegisterService {

  public static REGISTER_USER_EVENT = 'event/userEventRegister';
  public static CHECK_USER_REGISTRATION = 'event/isUserRegistered/{userId}/{eventId}';

  private _isRegistered: boolean = false;

  constructor(private restService: RestService,
              private userService: UserService,
              private matDialog: MatDialog) { }

  registerUser(eventId) {
    const params = { userId: this.userService.user.userId, eventId: eventId };
    this.restService.post(Utils.buildRestApiURL(SharedService.APP_LANGUAGE, EventRegisterService.REGISTER_USER_EVENT), params).subscribe((resp) => this.isRegistered = true);
  }

  checkIfUserRegistered(eventId) {
    const params = { userId: this.userService.user.userId, eventId: eventId };
    return this.restService.get(Utils.buildRestApiURL(SharedService.APP_LANGUAGE, EventRegisterService.CHECK_USER_REGISTRATION, params), null).subscribe(
      (resp) => {
        const ret = resp.getFirstData();
        this.isRegistered = ret;
        return ret;
      });
  }

  register(eventDetails): void {
    const dialogConf = new MatDialogConfig();
    dialogConf.autoFocus = true;
    dialogConf.width = '60%';
    dialogConf.maxHeight = '80%';
    dialogConf.panelClass = 'custom-modal-container';
    dialogConf.data = eventDetails;

    const dialogRef = this.matDialog.open(EventRegisterDialogComponent, dialogConf);
    dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.registerUser(eventDetails.id);
        }
    });
}


  get isRegistered() {
    return this._isRegistered;
  }

  set isRegistered(value) {
    this._isRegistered = value;
  }

}
