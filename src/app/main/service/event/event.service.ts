import { Injectable } from '@angular/core';
import { RestService } from '../rest/rest.service';
import { CompaniesService } from '../companies/companies.service';
import { SharedService } from '../shared.service';
import { Observable } from 'rxjs';
import { isNullOrUndefined } from 'util';
import { map } from 'rxjs/operators';
import { FormFieldsResponse } from '../rest/form-fields-response';
import { Utils } from 'app/main/utils/utils';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  public static SAVE_EVENT_FIELDS = '{company}/event/save/{event}';
  public static GET_EVENT_FIELDS = '{company}/event/getFields/{stepNo}';
  public static GET_EVENTS = '{company}/event/get/{event}';
  public static DELETE_EVENT = '{company}/event/delete/{event}';
  public static GET_ALL_CLIENT_EVENTS = 'event/getAll';
  public static GET_COMPANY_EVENTS = 'company/page/{company}';
  public static GET_EVENT_DETAILS = 'event/{event}';

  constructor(private restService: RestService,
    private companyService: CompaniesService,
    private sharedService: SharedService) {

  }

  getEventDetails(eventId: number, page: number = 1, search = null, categories = null): Observable<any> {
    const params = { event: eventId };
    let httpParams = new HttpParams().set('page', page.toString());
    if (!isNullOrUndefined(search)) {
      httpParams = httpParams.set('search', search);
    }
    if (!isNullOrUndefined(categories)) {
      httpParams = httpParams.set('categories', categories);
    }
    return this.restService.get(Utils.buildRestApiURL(SharedService.APP_LANGUAGE, EventService.GET_EVENT_DETAILS, params), httpParams).pipe(map((resp) => resp.getFirstData()));
  }


  getCompanyEvents(companyId: number): Observable<any> {
    const params = { company: companyId };
    return this.restService.get(Utils.buildRestApiURL(SharedService.APP_LANGUAGE, EventService.GET_COMPANY_EVENTS, params)).pipe(map((resp) => resp.getFirstData()));
  }

  getAllClientEvents(): Observable<any[]> {
    return this.restService.get(Utils.buildRestApiURL(SharedService.APP_LANGUAGE, EventService.GET_ALL_CLIENT_EVENTS)).pipe(map((resp) => resp.getData()));
  }

  deleteEvent(eventId: number): Observable<boolean> {
    const params = { event: eventId };
    return this.restService.post(Utils.buildRestApiURL(SharedService.APP_LANGUAGE, EventService.DELETE_EVENT, params), null).pipe(map((resp) => true));
  }

  getEvents(eventId?: number): Observable<any> {
    const params = { company: this.companyService.company.id, event: null };
    if (!isNullOrUndefined(eventId) && eventId > -1) {
      params['event'] = eventId;
    }
    return this.restService.get(Utils.buildRestApiURL(SharedService.APP_LANGUAGE, EventService.GET_EVENTS, params)).pipe(map((resp) => resp.getData()));
  }

  saveFields(values, eventId?): Observable<any> {
    const params = { company: this.companyService.company.id, event: null };
    if (!isNullOrUndefined(eventId) && eventId > -1) {
      params['event'] = eventId;
    }
    return this.sharedService.saveFormWithResponse(EventService.SAVE_EVENT_FIELDS, values, params).pipe(map((resp) => resp.getFirstData().event_id));
  }

  getBasicFields(eventId = -1): Observable<FormFieldsResponse> {
    return this.getFields(eventId, 1);
  }

  getSpecificFields(eventId = -1): Observable<FormFieldsResponse> {
    return this.getFields(eventId, 2);
  }

  getTypesFields(eventId = -1): Observable<FormFieldsResponse> {
    return this.getFields(eventId, 3);
  }

  getEventShop(eventId = -1) {
    return this.getFields(eventId, 5);
  }

  getEventAuction(eventId = -1) {
    return this.getFields(eventId, 4);
  }

  getEventInfo(eventId = -1) {
    return this.getFields(eventId, 6);
  }

  getFields(eventId, stepNo = 1): Observable<FormFieldsResponse> {

    let url = EventService.GET_EVENT_FIELDS;

    const params = {
      company: this.companyService.company.id,
      stepNo: stepNo
    };

    if (!isNullOrUndefined(eventId) && eventId > -1) {
      url += '/' + eventId;
    }
    return this.sharedService.getFormFields(url, params);
  }
}
