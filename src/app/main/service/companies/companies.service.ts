import { Injectable } from '@angular/core';
import { Utils } from '../../utils/utils';
import { SharedService } from '../shared.service';
import { map } from 'rxjs/operators';
import { User } from '../user/user';
import { RestService } from '../rest/rest.service';
import { Observable } from 'rxjs';
import { Company } from './company';
import { UserService } from '../user/user.service';

@Injectable({
    providedIn: 'root'
})
export class CompaniesService {

    private static GET_USER_COMPANIES = 'user/companys';
    public static GET_ADD_COMPANY_FORM_FIELDS_URL = 'company/getFields';
    public static GET_EDIT_COMPANY_FORM_FIELDS_URL = 'company/{company}/getFields';
    public static GET_COMPANY_DETAILS = 'company/getCompany/{company}';

    public static SAVE_COMPANY_URL = 'company/create';
    public static SAVE_EDITED_COMPANY_URL = 'company/update/{company}'

    private _company: Company = null;

    constructor(private restService: RestService,
        private userService: UserService) {
    }

    getCompanyDetails(compId: number): Observable<any> {
        return this.restService.get(Utils.buildRestApiURL(SharedService.APP_LANGUAGE,
            CompaniesService.GET_COMPANY_DETAILS,
            { company: compId }))
            .pipe(map(res => {
                if (!res.isEmpty()) {
                    return res.getFirstData();
                }
            }));
    }

    getUserCompanies(): Observable<Company[]> {
        return this.restService.get(Utils.buildRestApiURL(SharedService.APP_LANGUAGE, CompaniesService.GET_USER_COMPANIES))
            .pipe(map(res => {
                return res.getData().map(company => {
                    let comp = new Company();
                    comp.id = company.id;
                    comp.name = company.company_name;
                    comp.registrationNumber = company.registration_number;
                    return comp;
                });
            }));
    }

    get company(): Company {
        return this._company;
    }

    set company(value: Company) {
        this._company = value;
        this.userService.setCurrentUser(this._company);
    }



}
