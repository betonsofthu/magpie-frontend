import { BaseUser } from "../user/base-user";

export class Company extends BaseUser{
    private _id;
    private _registrationNumber;

    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
    }

    get registrationNumber() {
        return this._registrationNumber;
    }

    set registrationNumber(value) {
        this._registrationNumber = value;
    }

}
