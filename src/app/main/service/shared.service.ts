import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { FormFieldsResponse } from './rest/form-fields-response';
import { map } from 'rxjs/operators';
import { RestService } from './rest/rest.service';
import { Constants } from '../utils/constants';
import { Utils } from '../utils/utils';
import { RouteInfo } from '../../components/sidebar/route-info';
import { Route } from '@angular/router';
import { UserService } from './user/user.service';
import { CompaniesService } from './companies/companies.service';
import { RestResponse } from './rest/rest-response';
import { isNullOrUndefined } from 'util';
import { HttpParams } from '@angular/common/http';


@Injectable({
    providedIn: 'root'
})
export class SharedService {

    public static GET_USER_FORM_FIELDS_URL = 'user/fields';

    public static SAVE_USER_PROFILE_URL = 'user/update';

    public static APP_LANGUAGE = 'hu';


    public static ADD_TO_FAVORITE = 'user/favorite/create';
    public static REMOVE_FROM_FAVORITE = 'user/favorite/delete';
    public static GET_FAVORITE_ITEMS = 'user/favorite/{type}';

    private _selectedLayout: string = Constants.USER_LAYOUT;

    private layoutChangedSubject: ReplaySubject<string>;

    constructor(private restService: RestService) {
        this.layoutChangedSubject = new ReplaySubject(1);
    }

    get layoutChangedEvent(): Observable<string> {
        return this.layoutChangedSubject.asObservable();
    }

    changeLayout(layout: string): void {
        this.selectedLayout = layout;
        this.layoutChangedSubject.next(layout);
    }


    getFavorites(type, page = 1): Observable<any> {
        const params = { type: type };
        const httpParams = new HttpParams().set('page', page.toString());
        return this.restService.get(Utils.buildRestApiURL(SharedService.APP_LANGUAGE, SharedService.GET_FAVORITE_ITEMS,params), httpParams).pipe(map((resp) => resp.getFirstData()));
    }

    addToFavorite(itemId, type: 'event'|'product', relationId = null): Observable<any> {
        const params = { item_id: itemId, type: type };
        if (!isNullOrUndefined(relationId)){
            params['relation_id'] = relationId;
        }
        return this.restService.post(Utils.buildRestApiURL(SharedService.APP_LANGUAGE, SharedService.ADD_TO_FAVORITE), params).pipe(map((resp) => resp.getFirstData()));
    }

    removeFromFavorite(itemId, type: 'event'|'product', relationId = null): Observable<any> {
        const params = { item_id: itemId, type: type };
        if (!isNullOrUndefined(relationId)){
            params['relation_id'] = relationId;
        }
        return this.restService.post(Utils.buildRestApiURL(SharedService.APP_LANGUAGE, SharedService.REMOVE_FROM_FAVORITE), params).pipe(map((resp) => resp.getFirstData()));
    }


    getFormFields(url, urlParams?): Observable<FormFieldsResponse> {
        return this.restService.get(Utils.buildRestApiURL(SharedService.APP_LANGUAGE, url, urlParams))
            .pipe(map(res => {
                const result = res.getFirstData();
                return <FormFieldsResponse>{ formId: result['form']['id'], fields: result['fields'] };
            }));
    }

    saveForm(url, params, urlParams?): Observable<boolean> {
        return this.restService.post(Utils.buildRestApiURL(SharedService.APP_LANGUAGE, url, urlParams), params)
            .pipe(map(res => {
                return true;
            }));
    }

    saveFormWithResponse(url, params, urlParams?): Observable<RestResponse> {
        return this.restService.post(Utils.buildRestApiURL(SharedService.APP_LANGUAGE, url, urlParams), params);
    }

    getLayoutMenus(url): Observable<RouteInfo[]> {
        return this.restService.get(Utils.buildRestApiURL(SharedService.APP_LANGUAGE, url))
            .pipe(map(res => {
                const filteredResult = res.getData().filter((route) => {
                    return route.title !== 'Admin';
                });
                return filteredResult.map((menuItem, index) => {
                    const mainRoute = <RouteInfo>{ path: menuItem.route, title: menuItem.title, icon: 'menu', main: index === 0 };
                    if (!isNullOrUndefined(menuItem.submenu)) {
                        mainRoute.submenus = [];
                        menuItem.submenu.forEach(subMenu => {
                            mainRoute.submenus.push(<RouteInfo>{ path: subMenu.route, title: subMenu.title, icon: 'menu', main: false });
                        });
                    }

                    return mainRoute;
                });
            }));
    }

    isUserLayout(): boolean {
        return this.selectedLayout === Constants.USER_LAYOUT;
    }

    isCompanyLayout(): boolean {
        return this.selectedLayout === Constants.COMPANY_LAYOUT;
    }

    isAdminLayout(): boolean {
        return this.selectedLayout === Constants.ADMIN_LAYOUT;
    }

    get selectedLayout(): string {
        return this._selectedLayout;
    }

    set selectedLayout(value: string) {
        this._selectedLayout = value;
    }
}
