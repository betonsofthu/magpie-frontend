import {RestResponse} from '../rest/rest-response';
import { BaseUser } from './base-user';
import { isNullOrUndefined } from 'util';

export class User extends BaseUser{

    private _userId: number;
    private _roles: string[];
    private _email: string;
    private _lang: string;

    public static create(response: any): User {
        return new User(response);
    }

    get lang(): string {
        return this._lang;
    }

    set lang(value: string) {
        this._lang = value;
    }

    get userId(): number {
        return this._userId;
    }

    set userId(value: number) {
        this._userId = value;
    }
   
    get role(): string[] {
        return this._roles;
    }

    set role(value: string[]) {
        this._roles = value;
    }

    get email(): string {
        return this._email;
    }

    set email(value: string) {
        this._email = value;
    }

    get isAdmin(): boolean {
        let isAdmin = false;
        this._roles.forEach((role) => {
            if (role === 'admin'){
               isAdmin = true;
            }
        });
        return isAdmin;
    }

    private constructor(response: any) {
        super();
        this.name = response.first_name + ' ' + response.last_name;
        this._lastName = response.last_name;
        this._firstName = response.first_name;
        this._email = response.email;
        this._avatar = response.avatar;
        if (!isNullOrUndefined(response.roles)){
            this._roles = response.roles.map((role) => {
                return role.name;
            });
        }
        this._lang = response.lang;
        this._userId = response.id;
    }
}
