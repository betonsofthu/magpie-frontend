import {Injectable} from '@angular/core';
import {Subject, Observable} from 'rxjs';
import {User} from './user';
import { RestService } from '../rest/rest.service';
import { Utils } from 'app/main/utils/utils';
import { SharedService } from '../shared.service';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class AdminService {

    private static GET_USERS = 'kiosk/users';
    private static LOGIN_TO_USER = 'kiosk/create/{user}';

    private subject: Subject<User> = new Subject<User>();

    constructor(private restService:RestService) {
    }

    loginToUser(userId: number): Observable<any> {
        const params = { user: userId };
        return this.restService.post(Utils.buildRestApiURL(SharedService.APP_LANGUAGE, AdminService.LOGIN_TO_USER, params), null).pipe(map((resp) => resp.getFirstData()));
      }

    subjectObservable(): any {
        return this.subject.asObservable();
    }

    selectedUserByAdmin(user: User): void {
        sessionStorage.setItem('userId', user.userId.toString());
        this.subject.next(user);
    }

    unsubscribe(): void {
        console.log('leiratkozás');
        this.subject.complete();
    }
}
