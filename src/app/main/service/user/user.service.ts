import {Injectable} from '@angular/core';
import {User} from './user';
import {RestService} from '../rest/rest.service';
import {BehaviorSubject, Observable, ReplaySubject} from 'rxjs';
import {map} from 'rxjs/operators';
import {FormFieldsResponse} from '../rest/form-fields-response';
import {Utils} from '../../utils/utils';
import {SharedService} from '../shared.service';
import {isNullOrUndefined} from 'util';
import {AuthenticationService} from '../authentication/authentication.service';
import {TranslateService} from '@ngx-translate/core';
import { Company } from '../companies/company';
import { HttpParams } from '@angular/common/http';

type UserType = User | Company;

@Injectable({
    providedIn: 'root'
})
export class UserService {

    private static GET_USER_PATH = 'user/getUserData';
    private static GET_USERS = 'kiosk/users';

    private _user: User = null;
    private _selectedUser: UserType = null;

    private selectedUserChangedSubject: ReplaySubject<UserType>;

    constructor(private restService: RestService,
                private authService: AuthenticationService,
                private translate: TranslateService) {
        this.selectedUserChangedSubject = new ReplaySubject(1);
    }

    get userChangedEvent(): Observable<UserType> {
        return this.selectedUserChangedSubject.asObservable();
    }

    setCurrentUser(user: UserType) {
        this.selectedUser = user;
        this.selectedUserChangedSubject.next(this.selectedUser);
    }

    backToUser() {
        this.setCurrentUser(this.user);
    }

    goToAdmin() {
        this.setCurrentUser(this.user);
    }

    
    getAllUsers(page = 1): Observable<any> {
        const params = (new HttpParams()).set('page',page.toString());
        return this.restService.get(Utils.buildRestApiURL(SharedService.APP_LANGUAGE, UserService.GET_USERS), params).pipe(map((resp) => {
            return resp.getFirstData();
        }));
      }
    

    getUserDetails(force = false): Observable<User> {

        if (!isNullOrUndefined(this.user) && !force) {
            return Observable.of(this.getCachedUser());
        }

        if (!this.authService.isTokenPresent()) {
            return Observable.of(null);
        }
        return this.userDetails();
    }

    userDetails(): Observable<User> {
        return this.restService.get(Utils.buildRestApiURL(SharedService.APP_LANGUAGE, UserService.GET_USER_PATH))
            .pipe(map(res => {
                this.user = User.create(res.getFirstData());
                SharedService.APP_LANGUAGE = this.user.lang;
                sessionStorage.setItem('currentUser', JSON.stringify(res.getFirstData()));
                this.translate.setDefaultLang(SharedService.APP_LANGUAGE);
                this.setCurrentUser(this.user);
                return this.user;
            }));
    }

    getUserDetailsForce(): Observable<User> {
        return this.userDetails();
    }


    getCachedUser(): User {
        const currentUser: User = JSON.parse(sessionStorage.getItem('currentUser'));
        return currentUser;
    }

    get user(): User {
        return this._user;
    }

    set user(value: User) {
        this._user = value;
        this.setCurrentUser(this._user);
    }

    get selectedUser(): UserType {
        return this._selectedUser;
    }

    set selectedUser(value: UserType) {
        this._selectedUser = value;
    }
}
