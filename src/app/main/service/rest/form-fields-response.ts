export interface FormFieldsResponse {
    formId: number;
    fields: any[];
}
