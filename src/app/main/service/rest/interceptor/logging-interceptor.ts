import {HttpEvent, HttpEventType, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {tap} from 'rxjs/operators';
import {LoggerService} from '../../logger/logger.service';

@Injectable()
export class LoggingInterceptor implements HttpInterceptor {

    constructor(private logger: LoggerService) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.logger.fine(`${req.method} request to ${req.url}`);
        return next
            .handle(req)
            .pipe(
                tap((ev: HttpEvent<any>) => {
                    if (HttpEventType.Response === ev.type) {
                        this.logger.fine(`${req.method} response of ${ev.url}: \n${JSON.stringify(ev.body, null, 2)}`);
                    }
                })
            );
    }

}
