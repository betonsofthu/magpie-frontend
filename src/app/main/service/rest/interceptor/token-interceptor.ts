import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {AuthenticationService} from '../../authentication/authentication.service';
import {catchError, switchMap, tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {isNullOrUndefined} from 'util';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    isRefreshingToken = false;

    constructor(private authService: AuthenticationService,
                private router: Router) {
    }

// intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
//     if (this.authService.isTokenPresent()) {
//         req = req.clone({setHeaders: {Authorization: this.authService.getAuthToken()}});
//     }
//     return next.handle(req);
// }

    addToken(req: HttpRequest<any>, token: string): HttpRequest<any> {
        return req.clone({setHeaders: {Authorization: token}});

    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        if (!isNullOrUndefined(this.authService.getAuthToken())) {
            request = this.addToken(request, this.authService.getAuthToken());
        }
        return next.handle(request).pipe(
            catchError((error: Error) => {
                if (error instanceof HttpErrorResponse) {
                    console.log('err', error);
                    switch (error.status) {
                        case 401:
                            return this.handle401Error(request, next);
                        default:
                            return throwError(error);
                    }

                } else {
                    return throwError(error);
                }
            })
        );
    }

    handle401Error(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        console.log('handle 401');
        if (!this.isRefreshingToken) {
            this.isRefreshingToken = true;

            return this.authService.refreshToken().pipe(
                switchMap((newToken: string) => {
                    console.log('newToken', newToken);
                    if (newToken) {
                        return next.handle(this.addToken(request, newToken));
                    }

                    this.authService.logout();
                }),
                catchError((error) => {
                    console.log('error', error);
                    this.authService.logout();
                    return throwError(error);
                }),
                tap(data => console.log(data))
            );
        }
        return next.handle(this.addToken(request, this.authService.getAuthToken()));
    }

}




