export class CustomError extends Error {

    readonly errorCode: string | number;
    readonly errorMessage: string;

    constructor(errorCode: string | number, errorMessage: string) {
        super();
        Object.setPrototypeOf(this, CustomError.prototype);
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public print(): void {
        console.error(this.errorCode);
        console.error(this.errorMessage);
        console.error(this.stack);
    }

}
