import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {environment as env} from '../../../../environments/environment';
import {RestResponse} from './rest-response';
import {Observable, pipe, UnaryFunction} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {LoggerService} from '../logger/logger.service';
import {RequestError} from './request-error';
import {isNullOrUndefined} from 'util';
import {FetchRestResponse} from './FetchRestResponse';
import {NotificationService} from '../../notification/notification.service';


@Injectable({
    providedIn: 'root'
})
export class RestService {

    constructor(private http: HttpClient,
                private logger: LoggerService,
                private notificationService: NotificationService) {
    }

    public get(url: string, params?: HttpParams): Observable<RestResponse> {
        return this.http.get<any>(env.restURL + url, {params: params})
            .pipe(this.processResponse());
    }

    public post(url: string, body: any, options: RequestOptions = {}): Observable<RestResponse> {
        return this.http.post<any>(env.restURL + url, body, options)
            .pipe(this.processResponse());
    }

    private processResponse(): UnaryFunction<Observable<any>, Observable<RestResponse>> {
        return pipe(
            map(RestResponse.create),
            tap(this.checkResponse),
            catchError((err: any) => this.handleError(err))
        );
    }

    private checkResponse(response: RestResponse): void {
        if (!response.isSuccess()) {
            throw response;
        }
    }

    public getWithClass(url: string, params?: HttpParams):
        Observable<FetchRestResponse> {
        return this.http.get<any>(env.restURL + url, {params: params})
            .pipe(this.processResponseWithClass());
    }

    public postWithClass(url: string, body: any, options: RequestOptions = {}):
        Observable<FetchRestResponse> {
        return this.http.post<any>(env.restURL + url, body, options)
            .pipe(this.processResponseWithClass());
    }

    private processResponseWithClass(): UnaryFunction<Observable<any>,
        Observable<FetchRestResponse>> {
        return pipe(
            map(FetchRestResponse.create),
            tap(this.checkResponseWithClass),
            catchError((err: any) => this.handleError(err))
        );
    }

    private checkResponseWithClass(response: FetchRestResponse): void {
        if (!response.isSuccess()) {
            throw response;
        }
    }


    private handleError(err: any): never {
        this.logger.error(err);
        if (!isNullOrUndefined(err.errorMessage)) {
            this.notificationService.error(err.errorMessage);
        }
        throw RequestError.create(err) || err;
    }

}

export interface RequestOptions {
    headers?: HttpHeaders | {
        [header: string]: string | string[];
    };
    observe?: 'body';
    params?: HttpParams | {
        [param: string]: string | string[];
    };
    reportProgress?: boolean;
    responseType?: 'json';
    withCredentials?: boolean;
}

