import {isNullOrUndefined} from 'util';

export class RestResponse {

    private readonly _status: Status;
    private readonly _errorCode: string;
    private readonly _errorMessage: string;
    private readonly _data: any[];

    public static create(rawResponse: any): RestResponse {
        return new RestResponse(rawResponse);
    }

    constructor(rawResponse: any) {
        this._status = rawResponse.status;
        this._errorCode = rawResponse.error_code;
        this._errorMessage = rawResponse.error_message;
        this._data = rawResponse.data;
    }

    public isSuccess(): boolean {
        return this._status === Status.OK;
    }

    public getData(): any[] {
        return this._data;
    }

    public getFirstData(): any {
        return this._data[0];
    }

    public isEmpty(): boolean {
        return isNullOrUndefined(this._data) || this._data.length === 0;
    }

    public isNotEmpty(): boolean {
        return !this.isEmpty();
    }

    get status(): Status {
        return this._status;
    }

    get errorCode(): string {
        return this._errorCode;
    }

    get errorMessage(): string {
        return this._errorMessage;
    }


}

enum Status {
    OK = 'OK',
    FAILED = 'FAILED'
}
