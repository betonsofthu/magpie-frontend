import { Injectable } from '@angular/core';
import { RestService } from '../rest/rest.service';
import { Utils } from 'app/main/utils/utils';
import { SharedService } from '../shared.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CompaniesService } from '../companies/companies.service';
import { HttpParams } from '@angular/common/http';
import { isNullOrUndefined } from 'util';

export interface ProductImage {
  image: string | ArrayBuffer;
  path: string;
  title: string;
  name: string;
  file: File;
}


export interface Catalog {
  id: number;
  title: string;
  active: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class CatalogService {

  public static GET_ALL_CATALOGS = '{company}/catalog/get';
  public static GET_CATALOG = '{company}/catalog/get/{catalog}';
  public static SAVE_CATALOG = '{company}/catalog/create'
  public static UPDATE_CATALOG = '{company}/catalog/update/{catalog}';
  public static UPLOAD_FILE = '{company}/catalog/mapping'
  public static SAVE_MAPPING = '{company}/product/bulk'
  public static GET_PRODUCTS_WITH_PAGINATE = '{company}/product/getProducts/{catalog}/{limit}'
  public static GET_PRODUCT = '{company}/product/getFields/{stepNo}';
  public static UPDATE_PRODUCT = '{company}/product/save/{catalog}';
  public static REMOVE_CATALOG = '{company}/catalog/delete/{catalog}';
  public static REMOVE_PRODUCT = '{company}/product/delete/{product}';
  public static GET_PRODUCT_INFO = 'product/{product}/{event}';

  constructor(private restService: RestService,
    private companyService: CompaniesService,
    private sharedService: SharedService) { }

    getProductDetails(prodId: number, eventId: number): Observable<any> {
      return this.restService.get(Utils.buildRestApiURL(SharedService.APP_LANGUAGE, CatalogService.GET_PRODUCT_INFO, {product: prodId, event: eventId})).pipe(map((resp) => resp.getFirstData()));
    }
    

  saveProduct(catalogId, fields, productId?): Observable<any> {
    let url = Utils.buildRestApiURL(SharedService.APP_LANGUAGE,
      CatalogService.UPDATE_PRODUCT,
      {
        company: this.companyService.company.id,
        catalog: catalogId
      });
    if (!isNullOrUndefined(productId) && productId > -1) {
      url += '/' + productId;
    }
    const params = fields;
    return this.restService.post(url, params)
      .pipe(map(res => {
        return res.getFirstData().product_id;
      }));
  }

  getProductImages(productId): Observable<any> {
    return this.getProduct(productId, 2).pipe(map(res => {
      res.fields = res.fields.map((prodField) => {
        return <ProductImage>{
          title: prodField.title,
          name: prodField.name,
          path: prodField.value,
          image: prodField.value,
          file: null
        }
      });
      return res;
    }));
  }

  getProduct(productId, stepNo = 1): Observable<any> {

    let url = CatalogService.GET_PRODUCT;

    const params = {
      company: this.companyService.company.id,
      stepNo: stepNo
    };

    if (!isNullOrUndefined(productId) && productId > -1) {
      url += '/' + productId;
    }
    return this.sharedService.getFormFields(url, params).pipe(map(res => {
      res.fields = res.fields.map((pField) => {
        return {
          title: pField.title,
          name: pField.name,
          value: pField.values && pField.values[0] ? pField.values[0] : ''
        }
      });
      return res;
    }));
  }

  getProducts(catalogId, limit, page = 1): Observable<any> {
    const url = Utils.buildRestApiURL(SharedService.APP_LANGUAGE,
      CatalogService.GET_PRODUCTS_WITH_PAGINATE,
      {
        company: this.companyService.company.id,
        catalog: catalogId,
        limit: limit
      });
    const params = new HttpParams().set('page', page.toString());
    return this.restService.get(url, params)
      .pipe(map(res => {
        return res.getFirstData();
      }));
  }

  saveMapping(catalogId, mappedFields: any): Observable<any> {
    const url = Utils.buildRestApiURL(SharedService.APP_LANGUAGE,
      CatalogService.SAVE_MAPPING,
      { company: this.companyService.company.id });
    const params = {
      mapping: mappedFields,
      catalog_id: catalogId
    }
    return this.restService.post(url, params)
      .pipe(map(res => {
        return res.getData();
      }));
  }


  saveCatalog(title: string): Observable<any> {
    const params = { title: title };
    const url = Utils.buildRestApiURL(SharedService.APP_LANGUAGE,
      CatalogService.SAVE_CATALOG,
      { company: this.companyService.company.id });
    return this.restService.post(url, params)
      .pipe(map(res => {
        return res.getFirstData();
      }));
  }

  updateCatalog(catalogId: number, title: string, active = 1): Observable<any> {
    const params = { title: title, active: active };
    const url = Utils.buildRestApiURL(SharedService.APP_LANGUAGE,
      CatalogService.UPDATE_CATALOG,
      {
        company: this.companyService.company.id,
        catalog: catalogId
      });
    return this.restService.post(url, params)
      .pipe(map(res => {
        return res.getFirstData();
      }));
  }


  uploadFile(catalogId: number, file: File): Observable<any> {
    const formData = new FormData();
    formData.set('catalog_id', catalogId.toString());
    formData.set('file', file, file.name);
    const url = Utils.buildRestApiURL(SharedService.APP_LANGUAGE,
      CatalogService.UPLOAD_FILE,
      { company: this.companyService.company.id });
    return this.restService.post(url, formData)
      .pipe(map(res => {
        return res.getFirstData();
      }));
  }

  getAllCatalogs(): Observable<any> {
    const url = Utils.buildRestApiURL(SharedService.APP_LANGUAGE,
      CatalogService.GET_ALL_CATALOGS,
      { company: this.companyService.company.id });
    return this.restService.get(url)
      .pipe(map(res => {
        return res.getData();
      }));
  }

  getCatalog(catalogId): Observable<any> {
    const url = Utils.buildRestApiURL(SharedService.APP_LANGUAGE,
      CatalogService.GET_CATALOG,
      {
        company: this.companyService.company.id,
        catalog: catalogId
      });
    return this.restService.get(url)
      .pipe(map(res => {
        const catalog = res.getFirstData();
        return <Catalog>{ id: catalog.id, title: catalog.title, active: catalog.active };
      }));
  }

  removeCatalog(catalogId): Observable<any> {
    const url = Utils.buildRestApiURL(SharedService.APP_LANGUAGE,
      CatalogService.REMOVE_CATALOG,
      { company: this.companyService.company.id,
        catalog: catalogId });
    
    return this.restService.post(url,null)
      .pipe(map(res => {
        return res.getFirstData();
      }));
  }

  removeProduct(productId): Observable<any> {
    const url = Utils.buildRestApiURL(SharedService.APP_LANGUAGE,
      CatalogService.REMOVE_PRODUCT,
      { company: this.companyService.company.id,
        product: productId });
    
    return this.restService.post(url,null)
      .pipe(map(res => {
        return res.getFirstData();
      }));
  }

}
