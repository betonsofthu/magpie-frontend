import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthenticationService} from './authentication.service';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationGuard implements CanActivate {

    private readonly loginPath = '/login';

    constructor(private _router: Router,
                private authService: AuthenticationService) {
    }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if (this.authService.isTokenPresent()) {
            return true;
        } else {
            console.warn('Token is not present, redirecting to login.');
            this._router.navigate([this.loginPath], {queryParams: {requestUrl: state.url}});
            return false;
        }
    }

}
