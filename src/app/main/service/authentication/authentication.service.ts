import { Injectable } from '@angular/core';
import { isNullOrUndefined } from 'util';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { delay, map, switchMap } from 'rxjs/operators';
import { Observable, of, from } from 'rxjs';
import { RestService } from '../rest/rest.service';
import 'rxjs-compat/add/observable/of';
import 'rxjs-compat/add/operator/delay';
import { CookieService } from 'ngx-cookie-service';
import { NotificationService } from '../../notification/notification.service';
import { UserService } from '../user/user.service';
import { AuthService, GoogleLoginProvider, SocialUser } from 'angularx-social-login';


@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {

    public static LOGIN_URL = '/login/oauth';
    public static LOGIN_WITH_GOOGLE = '/login/socialite/google';
    public static REGISTER_URL = '/user/register';

    constructor(private router: Router,
        private restService: RestService,
        private _cookieService: CookieService,
        private notification: NotificationService,
        private socialAuthService: AuthService) {
    }

    public login(credentials: Credentials): Observable<void> {
        const requestOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/x-www-form-urlencoded',
            })
        };
        return this.restService.post(AuthenticationService.LOGIN_URL, this.toFormData(credentials), requestOptions)
            .pipe(map(res => {
                this.setAccessToken(res.getFirstData().access_token);
            }));
    }

    public setAccessToken(access_token) {
        sessionStorage.setItem('access_token', access_token);
    }

    public loginWithSocial(url): Observable<void> {
        const socialProvider = GoogleLoginProvider.PROVIDER_ID;
        return from(this.socialAuthService.signIn(socialProvider)).pipe(switchMap((result) => {
            return this.restService.post(AuthenticationService.LOGIN_WITH_GOOGLE, result).pipe(map((res) => { this.setAccessToken(res.getFirstData().access_token); }));
        }));
    }


    public register(firstName: string, lastName: string, email: string, lang: string, password: string): Observable<boolean> {
        const params = new HttpParams()
            .set('name', firstName + ' ' + lastName)
            .set('firstName', firstName)
            .set('lastName', lastName)
            .set('lang', lang)
            .set('email', email)
            .set('password', password);
        const requestOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/x-www-form-urlencoded',
            })
        };
        return this.restService.post(AuthenticationService.REGISTER_URL, params, requestOptions)
            .pipe(map(res => {
                this.notification.success('Register was successful');
                return true;
            }));
    }

    private toFormData({ user, password }: Credentials): string {
        return new HttpParams()
            .set('email', user)
            .set('password', password)
            .toString();
    }

    /**
     * ezt meg implamentálni kell,
     * ami most van kamu
     */
    public refreshToken(): Observable<string> {
        return of(this.getAuthToken()).pipe(delay(1000));
    }

    public isTokenPresent(): boolean {
        return !isNullOrUndefined(sessionStorage.getItem('access_token'));
    }

    public getAuthToken(): string {
        const idToken = sessionStorage.getItem('access_token');
        return `Bearer ${idToken}`;
    }

    public logout(): void {
        sessionStorage.removeItem('access_token');
        sessionStorage.removeItem('currentUser');
        this._cookieService.deleteAll();


        this.router.navigate(['/login']);
    }

    public removeAdminSelectedUserFromSession(): void {
        sessionStorage.removeItem('userId');
        console.log('minden törlése');
    }

    public isUserLoggedIn(): boolean {
        return !isNullOrUndefined(sessionStorage.getItem('currentUser'));
    }

}

export class Credentials {
    user: string;
    password: string;
}
