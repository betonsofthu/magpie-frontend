import {Component, OnInit} from '@angular/core';
import {UserService} from '../../main/service/user/user.service';
import {Constants} from '../../main/utils/constants';
import {SharedService} from '../../main/service/shared.service';
import {Router} from '@angular/router';
import {RouteInfo} from './route-info';
import {isNullOrUndefined} from 'util';
import { CompaniesService } from 'app/main/service/companies/companies.service';

declare const $: any;




@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
    menuItems: RouteInfo[];

    selectedLogoUrl = '';
    userName = '';

    constructor(private userService: UserService, private sharedService: SharedService, private router: Router) {
    }

    ngOnInit() {
        this.userService.userChangedEvent.subscribe(user => {
            this.selectedLogoUrl = user.avatar;
            this.userName = user.name;
            this.initMenuItems(this.sharedService.selectedLayout);
        });
    }

    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };

    initMenuItems(layout): void {
        let url = 'menus/';
        switch (layout) {
            case Constants.USER_LAYOUT:
                url += 'user';
                break;
            case Constants.COMPANY_LAYOUT:
                url += 'customer';
                break;
            case Constants.ADMIN_LAYOUT:
                url += 'admin';
                break;

        }
        this.sharedService.getLayoutMenus(url).subscribe(routeInfos => {
            let initNavigate = null;
            routeInfos.forEach(routerItem => {
                if (routerItem.main) {
                    initNavigate = routerItem.path;
                    routerItem.active = true;
                }
            });
            this.menuItems = routeInfos;
            if (initNavigate) {
                this.router.navigate([initNavigate]);
            }
        });

    }

    setActiveMenu(menuItem){
        this.menuItems.forEach((menuItem)=>{
            if (menuItem.active){
                menuItem.active = false;
            }
            if (!isNullOrUndefined(menuItem.submenus)){
                menuItem.submenus.forEach((smenuItem)=>{
                    if (smenuItem.active){
                        smenuItem.active = false;
                    }
                });
            }
        });
        menuItem.active = true;  
    }

    isSubMenuOpen(menuItem: RouteInfo){
        if (isNullOrUndefined(menuItem.submenus)){
            return false;
        }
        let isOpen = false;
        menuItem.submenus.forEach((subMenuItem) => {
            if (subMenuItem.active){
                isOpen = true;
            }
        })
        return menuItem.active || isOpen;
    }
}
