export interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
    layouts: string[];
    main: boolean;
    active?: boolean;
    submenus: RouteInfo[];
}
