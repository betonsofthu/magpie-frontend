import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../main/users/user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { CompaniesComponent } from '../../main/companies/companies.component';
import { CatalogsComponent } from 'app/main/catalogs/catalogs.component';
import { AddEditCatalogComponent } from 'app/main/catalogs/add-edit-catalog/add-edit-catalog.component';
import { CrudProductComponent } from 'app/main/catalogs/add-edit-catalog/crud-product.component';
import { MyEventsComponent } from 'app/main/events/my-events/my-events.component';
import { ClientEventsComponent } from 'app/main/events/client-events.component';
import { CompanyEventsComponent } from 'app/main/events/company-events.component';
import { EventDetailsComponent } from 'app/main/events/event-details/event-details.component';
import { ProductPageComponent } from 'app/main/events/event-details/product-page/product-page.component';
import { FavoritesComponent } from 'app/main/favorites/favorites.component';
import { KioskComponent } from 'app/main/kiosk/kiosk.component';

export const UserLayoutRoutes: Routes = [
    // {
    //   path: '',
    //   children: [ {
    //     path: 'dashboard',
    //     component: DashboardComponent
    // }]}, {
    // path: '',
    // children: [ {
    //   path: 'userprofile',
    //   component: UserProfileComponent
    // }]
    // }, {
    //   path: '',
    //   children: [ {
    //     path: 'icons',
    //     component: IconsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'notifications',
    //         component: NotificationsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'maps',
    //         component: MapsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'typography',
    //         component: TypographyComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'upgrade',
    //         component: UpgradeComponent
    //     }]
    // }
    { path: 'dashboard', component: DashboardComponent },
    { path: 'user-profile', component: UserProfileComponent },
    { path: 'company-profile', component: UserProfileComponent },
    { path: 'table-list', component: TableListComponent },
    { path: 'typography', component: TypographyComponent },
    { path: 'icons', component: IconsComponent },
    { path: 'maps', component: MapsComponent },
    { path: 'notifications', component: NotificationsComponent },
    { path: 'upgrade', component: UpgradeComponent },
    { path: 'my-companies', component: CompaniesComponent },
    { path: 'my-events', component: MyEventsComponent },
    { path: 'events', component: ClientEventsComponent },
    { path: 'catalogs', component: CatalogsComponent },
    { path: 'catalogs/add-edit', component: AddEditCatalogComponent},
    { path: 'crud-prod', component: CrudProductComponent},
    { path: 'company-events', component: CompanyEventsComponent},
    { path: 'event-details', component: EventDetailsComponent},
    { path: 'product-page', component: ProductPageComponent},
    { path: 'favorite/product', component: FavoritesComponent},
    { path: 'favorite/event', component: FavoritesComponent},
    { path: 'kiosk', component: KioskComponent}
];
