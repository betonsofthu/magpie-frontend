import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {DashboardComponent} from '../../dashboard/dashboard.component';
import {TableListComponent} from '../../table-list/table-list.component';
import {TypographyComponent} from '../../typography/typography.component';
import {IconsComponent} from '../../icons/icons.component';
import {MapsComponent} from '../../maps/maps.component';
import {NotificationsComponent} from '../../notifications/notifications.component';
import {UpgradeComponent} from '../../upgrade/upgrade.component';

import {
    MatButtonModule,
    MatInputModule,
    MatRippleModule,
    MatFormFieldModule,
    MatTooltipModule,
    MatSelectModule,
} from '@angular/material';
import {UsersModule} from '../../main/users/users.module';
import {CompaniesModule} from '../../main/companies/companies.module';
import {UserLayoutRoutes} from './user-layout.routing';
import {TranslateModule} from '@ngx-translate/core';
import { CatalogsModule } from 'app/main/catalogs/catalogs.module';
import { EventsModule } from 'app/main/events/events.module';
import { FavoritesModule } from 'app/main/favorites/favorites.module';
import { KioskModule } from 'app/main/kiosk/kiosk.module';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(UserLayoutRoutes),
        FormsModule,
        MatButtonModule,
        MatRippleModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatTooltipModule,
        UsersModule,
        CompaniesModule,
        EventsModule,
        TranslateModule,
        CatalogsModule,
        FavoritesModule,
        KioskModule
    ],
    declarations: [
        DashboardComponent,
        TableListComponent,
        TypographyComponent,
        IconsComponent,
        MapsComponent,
        NotificationsComponent,
        UpgradeComponent
    ]
})

export class UserLayoutModule {
}
