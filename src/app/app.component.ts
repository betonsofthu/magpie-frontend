import {Component, HostListener, OnInit, ɵbypassSanitizationTrustResourceUrl} from '@angular/core';
import {UserService} from './main/service/user/user.service';
import {OverlayContainer} from '@angular/cdk/overlay';
import {SharedService} from './main/service/shared.service';
import {Constants} from './main/utils/constants';
import {NavigationStart, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    selectedLayout = Constants.USER_LAYOUT;

    constructor(private sharedService: SharedService,
                private overlayContainer: OverlayContainer,
                private userService: UserService,
                private translate: TranslateService) {
        translate.setDefaultLang('en');
    }

    ngOnInit(): void {

        this.userService.getUserDetails(true).subscribe();

        this.overlayContainer.getContainerElement().classList.add(this.selectedLayout);
        this.sharedService.layoutChangedEvent.subscribe(layout => {
            this.overlayContainer.getContainerElement().classList.replace(this.selectedLayout, layout);
            this.selectedLayout = layout;
        });

    }
}
