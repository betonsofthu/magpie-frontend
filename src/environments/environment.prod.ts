const BASE_URL = 'http://magpie.betonsoft.hu';

const CONTEXT = '';

export const environment = {
  production: true,
  hmr: false,
  baseURL: BASE_URL,
  context: CONTEXT,
  restURL: BASE_URL + CONTEXT
};
