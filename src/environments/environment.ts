// const BASE_URL = 'http://localhost:8080';
// const BASE_URL = 'http://ppms';
// const BASE_URL = 'https://ppms.syncee.io';
const BASE_URL = 'http://magpie.betonsoft.hu';
// const BASE_URL = 'http://magpie-local';

const CONTEXT = '';
// export const CONTEXT = '/demo/public/rest/';

export const environment = {
  production: false,
  hmr: false,
  baseURL: BASE_URL,
  context: CONTEXT,
  restURL: BASE_URL + CONTEXT
};
